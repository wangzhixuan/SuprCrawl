<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<%=basePath%>js/ext-4.2/resources/css/ext-all-neptune.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/ext-all.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/extend/TabCloseMenu.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/locale/ext-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/app.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/util/common.js"></script>
	<style>
	body{background:#FFF;}
	.user-opper{color:#ADD2ED; background:#298BB8; text-align:right; padding-right:40px;}
	.user-info{ color:#F9EB08; padding:0 5px;}
	.user-role{padding:0 5px; color:#F93305;}
	.exit-a{color:#0B0B0B; margin:0 10px; cursor:pointer;}
	.exit-a:hover{ color:orange; }
	.index-img{width:400px; margin:170 auto;} 
	.index-img img{ width:100%;}
	.home-h1{line-heigh:50px; color#ccc;font-size:30px; text-align:center; padding:0; margin:0;}
	.home-h3{line-heigh:30px; color#ccc;font-size:24px; text-align:center; padding:5px 0; margin:0;}
	</style>
</head>

<body>   
 	
</body>  

</html>
