<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'test.jsp' starting page</title>
    <script type="text/javascript" src="<%=basePath%>js/zTree_v3/js/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="<%=basePath%>js/ellocate.js-master/jquery.ellocate.js"></script>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
<script type="text/javascript">
	(function($){
        $.fn.getQuery = function(options){
                o = $.extend({
                        type: 'xpath',        //生成类型  'xpath' or 'selector'
                        highLight : true, //选择的元素是否高亮显示
                        fullPath : false, //是否是全路径
                        preferenceAttr: 'id', //属性偏好 'id' or 'class'
                        bgColor: 'yellow',        //背景色
                        border:'yellow 1px solid',//边框
                        expansion: 3,//扩大边框
                }, options||{});
                if(o.highLight){
                        this.highLight(o);
                }
                
                var path = getPath(this, '');
                selector = path.replaceAll('/', '>').replaceAll('\\[', ':eq(').replaceAll('\\]', ')').replaceAll('\\:eq\\(\\@', '[').replaceAll('\'\\)', '\']');
                query = '/' + path;
                if(!o.fullPath){
                        query = '/' + query;
                }
                if(o.type=='selector'){
                        return selector;
                } else {
                        return query;
                }
        }


        this.getXpath = function(){
                return query;
        }


        this.getSelector = function(){
                return selector;
        }


        $.fn.highLight = function (options){
                op = $.extend({
                        bgColor: 'yellow',        //背景色
                        border:'yellow 1px solid',//边框
                        expansion: 3,//扩大边框
                }, options||{});
                $('body').append("<div id='abs-box' class='abs'> </div>");
                $('head').append("<style>.abs{position:absolute;zoom:1;pointer-events:none;z-index:-1;}</style>");
                var div = $('#abs-box');
                if(div != this){
                        var pos=this.offset(), em = op.expansion;
                        div.css({'left':pos.left-em,'top':pos.top-em,'width':this.width()+2*em,'height':this.height()+2*em});
                        div.css({'background-color':op.bgColor, 'border':op.border});
                }
        }
                
        function getPath (e, path){
                var tn = e.get(0).tagName;
                if(isNullOrEmpty(e) || isNullOrEmpty(tn)){
                        return path;
                }
                var attr = getAttr(e);
                tn = tn.toLowerCase() + attr;
                path = isNullOrEmpty(path) ? tn : tn + "/" + path;
                var parentE = e.parent();
                if(isNullOrEmpty(parentE) || (!o.fullPath && attr.substring(0, 5) == '[@id=')){
                        return path;
                }
                return getPath(parentE, path);
        }


        function getAttr (e){
                var tn = e.get(0).tagName;
                var id = e.attr('id'), clazz = e.attr('class');
                var hasId = !isNullOrEmpty(id), hasClazz = !isNullOrEmpty(clazz);
                id = "[@id='" + id + "']";
                clazz = "[@class='" + clazz + "']";
                if(hasId && hasClazz){
                        if(o.preferenceAttr.toLowerCase() == 'class'){
                                return clazz;
                        } else {
                                return id;
                        }
                } else if(hasId && !hasClazz) {
                        return id;
                } else if(!hasId && hasClazz) {
                        return clazz;
                } else {
                        if(e.siblings(tn).size() > 0) {
                                var i = e.prevAll(tn).size();
                                if(o.type=='xpath'){
                                        i++;
                                }
                                return '[' + i + ']';
                        } else {
                                return '';
                        }
                }
        }


        function isNullOrEmpty (o){
                return null==o || 'null'==o || ''==o || undefined==o;
        }


})(jQuery);


String.prototype.replaceAll = function(regx,t){   
        return this.replace(new RegExp(regx,'gm'),t);   
};


$(document).ready(function () {
 	$(document).click(function(e){
         e=e||window.event;
         var target=e.target||e.srcElement;
         var path = $(target).getQuery({
             type: 'xpath',        //生成类型:  'xpath' or 'selector', 默认是'xpath'
             preferenceAttr: 'class', // 属性偏好: 'id' or 'class', 默认是id
             highLight : true, //选择的元素是否高亮显示, 默认是true
             bgColor: 'red',        //背景色, 默认是'yellow'
             border:'black 3px solid',//边框, 默认是'yellow 1px solid'
             expansion: 2,//扩大边框, 默认是3
             fullPath: false //是否是全路径, 默认是false
         });
         alert(path);
         return false;
     });
});

</script>
  
<body>

<div class="article block untagged noline mb15" id="qiushi_tag_101977144">

<div class="author clearfix">
<a href="/users/26387168">
<img src="http://pic.qiushibaike.com/system/avtnew/2638/26387168/medium/20150308173808.jpg" alt="丶Angel。">
</a>
<a href="/users/26387168">丶Angel。 </a>
</div>

<div class="content" title="2015-03-09 10:13:41">

今天做地铁特挤，一男的在我后面蹭，突然感觉到一个硬硬的东西，老娘一个回身就是一个大嘴吧子。当我看到他手里拎着一袋萝卜的瞬间蒙了，还好我反应快直接说句你居然在外面有别的女人，正好到站，老娘直接跑下地铁，留着那哥们在人群中凌乱。。。。哥们对不住啊，妹不是故意的

</div>


<div class="stats clearfix">
<span class="stats-vote"><i class="number">6506</i> 好笑</span>
<span class="stats-comments">


<span class="dash"> · </span>
<i class="number">107</i> 回复



</span>
</div>
<div id="qiushi_counts_101977144" class="stats-buttons bar clearfix">
<ul class="clearfix">
<li id="vote-up-101977144" class="up">
<a href="javascript:voting(101977144,1)" class="voting" data-article="101977144" id="up-101977144" title="6926个顶">
<i class="iconfont" data-icon-actived="󰁡" data-icon-original="󰀟">󰀟</i>
<span class="number hidden">6926</span>
</a>
</li>
<li id="vote-dn-101977144" class="down">
<a href="javascript:voting(101977144,-1)" class="voting" data-article="101977144" id="dn-101977144" title="-420个拍">
<i class="iconfont" data-icon-actived="󰀠" data-icon-original="󰀠">󰀠</i>
<span class="number hidden">-420</span>
</a>
</li>

<li class="share">
<a href="javascript:void(0);" class="trigger">
<i class="iconfont" data-icon-actived="󰁠" data-icon-original="󰁟">󰁟</i>
</a>
<div class="sharebox">
<div class="up-arrow"></div>
<div id="bdshare" class="bdshare_t bds_tools get-codes-bdshare" data="">
<a class="bds_renren renren" href="#">
<i class="iconfont">󰁣</i>
人人网
</a>
<a class="bds_qzone qqzone" href="#">
<i class="iconfont">󰁐</i>
QQ空间
</a>
<a class="bds_tsina weibo" href="#">
<i class="iconfont">󰁌</i>
新浪微博
</a>
<a class="bds_tqq tqq" href="#">
<i class="iconfont">󰁏</i>
腾讯微博
</a>
</div>
</div>
</li>
</ul>
</div>












<div id="pager-new" class="clearfix pagebar" style="clear: both;border-bottom: 0; padding-bottom: 0; padding-top: 15px;border-top: 1px solid #ddd;">
<a href="#?s=4752935&amp;list=8hr" class="prev pager-pre pager-none-pre disabled">
<i class="iconfont">󰀢</i>
</a>
<a href="/article/101977406?s=4752935&amp;list=8hr" class="next pager-next">
下一条<i class="iconfont">󰀥</i>
</a>
</div>
<div class="tips clearfix" style="margin-top: -50px;padding-bottom: 10px;">
<span class="text">按方向键翻页</span>
<span class="key key-left">←</span>
<span class="key key-right">→</span>
</div>

</div>

</body>
</html>
