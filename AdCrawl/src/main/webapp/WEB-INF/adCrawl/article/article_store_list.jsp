<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<%=basePath%>js/ext-4.2/resources/css/ext-all-neptune.css" type="text/css"></link>
	<link rel="stylesheet" href="<%=basePath%>app/extend/example.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/ext-all.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/locale/ext-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/util/common.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/controller/ArticleStoreController.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/extend/examples.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/zTree_v3/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js"></script>
</head>

<style type="text/css">
	.x-grid-cell.user-caoji {
		background-color: red;
	}
	
	.x-grid-cell.user-admin { /* background-color: blue;  */
		
	}
	
	.img.someCss {
	}
	
	.emptyCss {
		visibility:hidden;
		disable:true;
	}
	
	.x-action-col-icon{
		margin:0 3px
	}
	.x-grid-view{position:relative;}
	.reason-tips{display:none;position:absolute; border:1px solid #ccc; width:160px; line-height:22px; color:#333; padding:5px;}
</style> 

<script type="text/javascript">
	Ext.onReady(function(){
	Ext.QuickTips.init();
	Ext.Loader.setConfig({
		enabled:true
	});
	Ext.application({
		name:"app",
		appFolder:"app",
		launch:function(){ 
			Ext.create("Ext.container.Viewport",{
				layout:'fit',
				items:[{
					xtype:"articleStoreGrid"
				}]
			});
		},
		controllers:[
			"ArticleStoreController"
		]
	});
});

var maxId;
var currentIds;
var flag = true;
// 定时查询仓库新增段子数  并弹出提示框
setInterval(function(){
	if(flag){
		// 第一次  获取最大值
		$.ajax({
          type: "post",
          url: "getArticleStoreCount",
          data: {
          },
          dataType: "json",
          success: function(data){
          	 var result = eval(data);
          	 maxId = result.data;
          	 flag = false;
           }
      }); 
	}else{
		$.ajax({
          type: "post",
          url: "getArticleStoreCount",
          data: {
          },
          dataType: "json",
          success: function(data){
          	 var result = eval(data);
          	 currentIds = result.data;
           }
      });
      
      	if(currentIds-maxId > 0){
			Ext.example.msg('',"有"+(currentIds-maxId)+"条新段子入库", '');
			maxId = currentIds;
      	}
	}
},10000); 

</script>

<body>   
 
 
</body>  

</html>
