<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<%=basePath%>css/public.css" type="text/css"></link>
<link rel="stylesheet" href="<%=basePath%>app/extend/example.css" type="text/css"></link>
<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0; border:none;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 610px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #298BB8; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #298BEF; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2;}
	.filedset-pad{padding:10px 5px 0;}
	.area01{margin:10px 0 0 300px;}
</style>
<script type="text/javascript" src="<%=basePath%>js/zTree_v3/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ext-4.2/ext-all.js"></script>
<script type="text/javascript" src="<%=basePath%>js/ext-4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript" src="<%=basePath%>app/util/common.js"></script>
<script type="text/javascript" src="<%=basePath%>app/controller/PayModeController.js"></script>
<script type="text/javascript" src="<%=basePath%>app/extend/examples.js"></script>

<script type="text/javascript">

// 根据类型获取站点
function getSiteByType(){
	var type = $("#type").val();
	empty($("#siteId"));
	empty($("#siteCategoryId"));
	$.ajax({
         type: "post",
         url: "getSiteListByType",
         data: {
         	type : type
         },
         dataType: "json",
         success: function(data){
	         var result = eval(data);
	         $("#siteId").html(result.data);
         }
    });
}

// 根据站点获取分类
function getCategoryBySite(){
	var siteId = $("#siteId").val();
	empty($("#siteCategoryId"));
	$.ajax({
         type: "post",
         url: "getCategoryListBySiteId",
         data: {
         	siteId : siteId
         },
         dataType: "json",
         success: function(data){
	         var result = eval(data);
	         $("#siteCategoryId").html(result.data);
         }
    });
}

function empty(id) {
		id.empty();
		id.append("<option value='-1'>=======请选择=======</option>");
}

// 新增爬虫任务
function addCrawl(){

	var dataParm = decodeURIComponent($(
							"#sdk_info_form").serialize(), true);
				dataParm = encodeURI(encodeURI(dataParm));
				$.ajax({
						url : './addCrawlInfo?data=' + Math.random(),
						data : dataParm,
						type : "post",
						dataType : "json",
						success : function(data) {
							if (data.resultCode == 'error') {
								Ext.example.msg('',data.errorInfo, '');
								return;
							} else if (data.resultCode == 'success') {
								// 弹出新增成功消息框 
								Ext.example.msg('', '操作成功', '');
								setTimeout(function(){
									// 刷新当前页面
									window.location.reload();
								},2000);
							}
						}
				});	
}

</script>


</head>
<body>
<form id="sdk_info_form" method="post" class="form">
	<div class="filedset-pad">
 		<fieldset>
    	<legend><font style="color:red">第一步、选择爬取类型</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">爬取类型：</label>
				<select id="type" name="type" onchange="getSiteByType()" style="width:200px">
					<option value="-1">=======请选择=======</option>
					<option value="1">段子</option>
				</select>
				--
				<select id="siteId" name="siteId" onchange="getCategoryBySite()" style="width:200px">
					<option value="-1">=======请选择=======</option>
				</select>
				--
				<select id="siteCategoryId" name="siteCategoryId" style="width:200px">
					<option value="-1">=======请选择=======</option>
				</select>
			</li>
		</ul>
		</fieldset>
		</div>
		<div class="filedset-pad">
		<fieldset>
    	<legend><font style="color:red">第二步、设置爬虫url</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">listUrl：</label> 
				<input type="text" style="width:632px" maxLength="100" class="fl" id="listUrl" name="listUrl">
			</li>
			
			<li>
				<label class="fl lab-left">start：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="start" name="start">
				<label class="fl lab-left" style="margin-left:132px;">end：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="end" name="end">
			</li>
			
			<li>
				<label class="fl lab-left">infoUrlXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlXpath" name="infoUrlXpath">
				<label class="fl lab-left" style="margin-left:132px;">listUrlRegular：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="listUrlRegular" name="listUrlRegular">
			</li>
			
			<li>
				<label class="fl lab-left">infoUrlPre：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlPre" name="infoUrlPre">
				<label class="fl lab-left" style="margin-left:132px;">infoUrlRegular：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoUrlRegular" name="infoUrlRegular">
			</li>
			
			<li>
				<label class="fl lab-left">infoContentXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoContentXpath" name="infoContentXpath">
				<label class="fl lab-left" style="margin-left:132px;">infoPicXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="infoPicXpath" name="infoPicXpath">
			</li>
			
			<li>
				<label class="fl lab-left">repeatFlagXpath：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="repeatFlagXpath" name="repeatFlagXpath">
				<label class="fl lab-left" style="margin-left:132px;">repeatMaxTimes：</label> 
				<input type="text" style="width:200px" maxLength="100" class="fl" id="repeatMaxTimes" name="repeatMaxTimes">
			</li>
			
		</ul>
		</fieldset>
		</div>
		<div class="filedset-pad">
		<fieldset>
    	<legend><font style="color:red">第三步、设置爬虫定时策略</font></legend>
		<ul class="sdklist-info">
			<li>
				<label class="fl lab-left">周期：</label> 
				<select id="crawlTime" name="crawlTime" style="width:200px">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
				</select>
				--
				<select style="width:200px">
					<option value="1">点</option>
				</select>
				<font style="color:red">(多个爬虫的爬取时间建议不要重叠,防止带宽不足抓取失败)</font>
			</li>
			
			<li>
				<label class="fl lab-left">休眠时间：</label> 
				<select name="sleepTime" id="sleepTime" style="width:200px">
					<option value="100">100毫秒</option>
					<option value="200">200毫秒</option>
					<option value="300">300毫秒</option>
					<option value="400">400毫秒</option>
					<option value="500">500毫秒</option>
					<option value="600">600毫秒</option>
					<option value="700">700毫秒</option>
					<option value="800">800毫秒</option>
					<option value="900">900毫秒</option>
					<option value="1000">1000毫秒</option>
				</select>
				<font style="color:red">(不同网站抓取休眠时间不同，防止请求过快抓取失败)</font>
			</li>	
		</ul>
		</fieldset>
			<a href="javascript:void(0)" class="save-btn fl area01" onclick="addCrawl()">保存</a>
	</div>
</form>
</body>
</html>
