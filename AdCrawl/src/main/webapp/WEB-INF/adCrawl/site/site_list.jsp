<%@ page language="java" import="java.util.*"  pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<base href="<%=basePath%>">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="<%=basePath%>js/ext-4.2/resources/css/ext-all-neptune.css" type="text/css"></link>
	<link rel="stylesheet" href="<%=basePath%>app/extend/example.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/ext-all.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ext-4.2/locale/ext-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/util/common.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/controller/SiteController.js"></script>
	<script type="text/javascript" src="<%=basePath%>app/extend/examples.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/zTree_v3/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>js/ajaxfileupload.js"></script>
</head>

<script type="text/javascript">	
	Ext.onReady(function(){
		Ext.QuickTips.init();
		Ext.Loader.setConfig({
			enabled:true
		});
		Ext.application({
			name:"app",
			appFolder:"app",
			launch:function(){ 
				Ext.create("Ext.container.Viewport",{
					layout:'fit',
					items:[{
						xtype:"siteGrid"
					}]
				});
			},
			controllers:[
				"SiteController"
			]
		});
	});
</script>

<body>   
 
 
</body>  

</html>
