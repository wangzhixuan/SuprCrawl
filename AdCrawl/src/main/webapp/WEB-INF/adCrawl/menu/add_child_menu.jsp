<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	body,div,input,label,h1,ul,li{margin: 0; padding: 0;}
	body, button, input, select, textarea {font: 12px "微软雅黑",arial,sans-serif;color:#2e2e2e;}
	table {border-collapse:collapse;border-spacing:0;}
	fieldset,img {border:0}
	address,caption,cite,code,dfn,strong,th,var{font-style:normal;font-weight:normal}
	ol,ul {list-style:none}
	caption,th {text-align:left}
	h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:normal}
	q:before,q:after {content:''}
	abbr,acronym { border:0}
	a{text-decoration:none;color:#999;outline:none;hide-focus:expression(this.hideFocus=true);}
	em{font-style:normal;}
	input,a,textarea{ outline:none;}
	a:hover{ text-decoration:underline;}
	html,body{width:100%;height:100%;}
	.clearfix:after{content:".";display:block;height:0;clear:both;visibility:hidden}
	.clearfix{display:inline-block}* html .clearfix{height:1%}.clearfix{display:block}
	.fl{float:left;_display:inline}
	.p20{ padding: 20px;}.pt15{padding-top: 15px;}.pl20{padding-left: 20px;}.pb10{padding-bottom: 10px;}
	.m20{margin-left: 10px;}.ml10{margin-left:10px;}.mt20{margin-top:20px;}
	.pr{position: relative;}
	.h1-title{ font-size: 24px; line-height: 50px; font-weight: normal;}
	.sdklist-info li{ clear: both; overflow: hidden; padding:10px 0;}
	label.lab-left{ width: 100px; line-height: 26px; font-size: 12px;}
	input[type="text"]{ line-height: 26px; width: 260px; border: 1px solid #ccc;}
	textarea{ width: 560px; resize:none; height: 120px; line-height: 24px; overflow: auto; border: 1px solid #ccc;}
	input[type="checkbox"]+label{ position: relative; top: -2px; padding-left: 5px;}
	.save-btn{ background: #49c39d; border-radius: 5px; font-size: 18px; padding: 10px 60px; color: #FFF;}
	.save-btn:hover{ background: #6ce6c0; text-decoration: none;}
	select{ line-height: 26px; height: 26px; width: 120px;}
	.input-file{ position: absolute; left:0; left: 0px;opacity:0; filter:alpha(opacity=0); line-height: 28px; height:28px; width:230px; cursor:pointer;}
	.browse-btn{line-height: 28px;padding: 0 15px; background: #808080; color: #FFF;text-align: center; height:28px;}
	.upload-btn{ line-height: 28px;padding: 0 15px; background: #49c39d; color: #FFF;text-align: center; border:0; cursor:pointer;height:28px;}
	a:hover{text-decoration: none;}
	input[type="text"].textfield{width: 174px;}
	fieldset{border:1px solid #a6c9e2; margin-top:10px;}
	.filedset-pad{padding:0 5px;}
</style>

<script type="text/javascript">
// 校验
function validate(){
		var menuName = $("#menuName").val();
		if(menuName == 0 || menuName == ''){
			Ext.example.msg('', '请填写菜单名称', '');
			return false;
		}
		
		var menuCode = $("#menuCode").val();
		if(menuCode == 0 || menuCode == ''){
			Ext.example.msg('', '请填写菜单编号', '');
			return false;
		}
		
	 	var parentId = $("#parentId").val();
		if(parentId == 0 || parentId == '0'){
			Ext.example.msg('', '请选择父菜单', '');
			return false;
		}
		
		return true;
	}


	//myMask.hide();
</script>
	
	
	
</head>
<body>
<form id="add_info_form" method="post" class="form" action=""  enctype="multipart/form-data">
	<div class="filedset-pad">
 		<fieldset>
    	<legend>新增子菜单</legend>
		<ul class="sdklist-info">
			<li>
				<label for="" class="fl lab-left">菜单名称：</label>
				<input type="text" class="fl" id="menuName" name="menuName" maxlength="20"/>
			</li>
			<li>
				<label for="" class="fl lab-left">菜单编号：</label>
				<input type="text" class="fl" id="menuCode" name="menuCode" maxlength="20"/>
			</li>
			<li>
				<label for="" class="fl lab-left">父菜单：</label>
				<select name="parentId" id="parentId" class="fl"  style="width:145px;">
					<option value="0">请选择</option>
					<c:forEach items="${menuList}" var="menu">
						<option value="${menu.id}">${menu.menuName}</option>
					</c:forEach>
				</select>
			</li>  
			<li>
				<label for="" class="fl lab-left">类别：</label>
				<select name="menuType" id="menuType" class="fl" style="width:145px;">
					<option value="1">无子菜单的图文列表</option>
					<option value="2">带子菜单的图文列表</option>
					<option value="3">带子菜单的图片列表</option>
					<option value="4">无子菜单的图片列表</option>
				</select>
			</li>
		</ul>
		</fieldset>
	</div>
</form>

</body>

</html>
