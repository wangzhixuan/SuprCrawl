Ext.define("app.store.FeePublishCensusStore",{
	extend:'Ext.data.Store',
	model: 'app.model.FeePublishCensusModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getpublishData',  // 查询
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});