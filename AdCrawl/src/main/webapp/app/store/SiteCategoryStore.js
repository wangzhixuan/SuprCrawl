Ext.define("app.store.SiteCategoryStore",{
	extend:'Ext.data.Store',
	model: 'app.model.SiteCategoryModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getSiteCategoryList',  // 查询
        //	update: 'updateAppList',  // 更新
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    listeners: {//用于在分页的时候追加查询参数   
    	"beforeload" : function(store) { 
    		var key = encodeURIComponent(Ext.getCmp("key").getValue());
    		
    		Ext.apply(store.proxy.extraParams, {
    			key : key
    		});
    	}      
    },
    autoLoad: true
});