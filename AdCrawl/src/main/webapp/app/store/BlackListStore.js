Ext.define("app.store.BlackListStore",{
	extend:'Ext.data.Store',
	model: 'app.model.BlackListModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getBlackList',  // 查询
        	update: '',  // 更新
        	create: '', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    listeners: {//用于在分页的时候追加查询参数   
    	"beforeload" : function(store) { 
    		var content = Ext.getCmp("content").getValue();
    		var type = Ext.getCmp("searType").getValue();
    		
    		Ext.apply(store.proxy.extraParams, {
    			content : encodeURIComponent(content),
    			type : type
    		});
    	}      
    },
    autoLoad: true
});