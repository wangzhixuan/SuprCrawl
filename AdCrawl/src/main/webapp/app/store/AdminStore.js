Ext.define("app.store.AdminStore",{
	extend:'Ext.data.Store',
	model: 'app.model.AdminModel',
	pageSize: 20, // 每页显示条数
    proxy: {
        type: 'ajax',
        api: {
        	read: 'getAdminList',  // 查询
        	update: 'updateAdminList',  // 更新
        	create: 'addAdminList', // 新增
        	destroy: ''
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'totalCount'
        },
        writer:{
        	type:'json'
        }
    },
    autoLoad: true
});