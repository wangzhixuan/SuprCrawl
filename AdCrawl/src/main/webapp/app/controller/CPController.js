Ext.define("app.controller.CPController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			"cpGrid button[id=add]" : {
				click : function(button) {
					var grid = this.getGrid(button);
					var store = grid.getStore();
					var edit = grid.editing;
					var model = store.model;
					// 新增模板
					var modelObj = {
						//userType:'2',
						state:'0',
						sing:true
					};
					var adminObj = new model(modelObj);
					edit.cancelEdit();
					var newRecords = store.getNewRecords(); 
					if(newRecords.length<1)
						store.insert(0, adminObj);
					
					edit.startEditByPosition({
						row : 0,
						column : 2
					});
				}
			},
			"cpGrid button[id=save]" : {
				click : function(button) {
					var grid = this.getGrid(button);
					var store = grid.getStore();
					var updateRecords = store.getUpdatedRecords(); // 更新的数据
					var newRecords = store.getNewRecords(); // 新增的数据
					
					if(newRecords.length == 1){
						
						// 新增单个
						var newData = newRecords[0].data;
						
						if(!validate(newData)){
							return;
						}
						
						// 新增单个
						Ext.Ajax.request({
							url : "addCPInfo",
							jsonData : Ext.encode(newRecords[0].data),
							type : "post",
							timeout : 4000,
							success : function(response, opts) {
								var result = eval('('+response.responseText+')');
								if(result.resultCode == 'success'){
									// 刷新grid
									store.load();
									Ext.example.msg('', '新增成功', '');
								}else if(result.resultCode == 'fail'){
									Ext.example.msg('', 'CP名称或者CP编码不能重复', '');
								}else{
									Ext.example.msg('', '新增失败', '');
								}
							}
						});
					}
//					else if(newRecords.length > 1){
//						store.sync({	// 同步数据到后台
//							success:function(){
//								Ext.Array.each(newRecords, function(model) {
//									model.commit(); // 前端提交  只是简单的修改前端红点样式
//								});
//								// 刷新grid
//								store.load();
//								Ext.example.msg('', '新增成功', '');
//							},
//							failure:function(){
//								Ext.example.msg('', '新增失败', '');
//							}
//						}); 
//					}
					
					if(updateRecords.length == 1){
						
						var newData = updateRecords[0].data;
						if(!validate(newData)){
							return;
						}
						
						// 更新单个
						Ext.Ajax.request({
							url : "updateCPInfo",
							jsonData : Ext.encode(updateRecords[0].data),
							type : "post",
							timeout : 4000,
							success : function(response, opts) {
								var result = eval('('+response.responseText+')');
								if(result.resultCode == 'success'){
									// 刷新grid
									store.load();
									//将修改表示初始化
									flag=-1;
									Ext.example.msg('', '更新成功', '');
								}else if(result.resultCode == 'fail'){
									Ext.example.msg('', 'CP名称或者CP编码不能重复', '');
								} else {
									Ext.example.msg('', '更新失败', '');
								}
							}
						});
					}else if(updateRecords.length > 1){
//					
					}
				}
			},
			"cpGrid":{
				edit:function(editor, e){ //监听edit事件
//					e.record.commit(); // 前端提交 没太大意义
					// 校验输入
					
				}
			}
		});
	},
	views  : ["CPGrid"],
	stores : ["CPStore"],
	models : ["CPModel","AppCategoryModel"]
});


function validateVerify(newData){
	var sing = true;
	var verifyMsg = newData.verifyMsg;
	var message = "";
	
	if(verifyMsg == "" || verifyMsg == null){
		message+= '审核失败错误信息不能为空,';
		sing = false;
	}
	
	if(!sing){
		Ext.example.msg('', message, '');
	}
	return sing;
}

function validate(newData){
	var sing = true;
	var cpName = newData.cpName;
	var cpCode = newData.cpCode;
	
	var message = "";
	
	if(cpName==""){
		message+= '客户名称不能为空,';
		sing = false;
	}
	if(!sing){
		Ext.example.msg('', message, '');
	}
	return sing;
}