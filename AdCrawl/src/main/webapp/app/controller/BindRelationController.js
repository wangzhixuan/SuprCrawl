Ext.define("app.controller.BindRelationController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["BindRelationGrid"],
	stores : ["BindRelationStore"],
	models : ["BindRelationModel","MenuModel","SiteModel","SiteCategoryModel"]
});