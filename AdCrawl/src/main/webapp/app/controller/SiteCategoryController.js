Ext.define("app.controller.SiteCategoryController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
	},
	views  : ["SiteCategoryGrid"],
	stores : ["SiteCategoryStore"],
	models : ["SiteCategoryModel"]
});