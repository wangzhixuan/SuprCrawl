Ext.define("app.controller.FeeCensusController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["FeeCensusGrid"],
	stores : ["FeeCensusStore"],
	models : ["FeeCensusModel","CPModel","ApplicationModel","ChannelModel",]
});