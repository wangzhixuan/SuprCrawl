Ext.define("app.controller.FeeCensusKController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["FeeCensusKGrid"],
	stores : ["FeeCensusKStore"],
	models : ["FeeCensusModel","CPModel","ApplicationModel","ChannelModel",]
});