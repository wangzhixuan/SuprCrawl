Ext.define("app.controller.FeePublishCensusController", {
	extend : "Ext.app.Controller",
	init : function() {
		this.getGrid = function(button) {
			return button.ownerCt.ownerCt;
		};
		this.control({
			
		});
	},
	views  : ["FeePublishCensusGrid"],
	stores : ["FeePublishCensusStore"],
	models : ["FeePublishCensusModel"]
});