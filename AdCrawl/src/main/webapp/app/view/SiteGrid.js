function showErrorMsg(msg){
	Ext.example.msg('', msg, '');
}

var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'key',
		xtype : 'textfield',
		name : 'key',
		fieldLabel : '关键字',
		labelWidth : 50,
		width : 360,
		emptyText : '==============请填写关键字============='
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("key").setValue("");
			search();
		}
	}]
});

function search(){
	var key = encodeURIComponent(Ext.getCmp("key").getValue());
	
	var siteStore = Ext.getCmp("siteGrid").getStore();
	siteStore.load({
		params : {
			key : key,
			page : 1,
			start : 0,
			limit : 20
        }
	});
	siteStore.currentPage=1
}

Ext.define("app.view.SiteGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.siteGrid",
			id : "siteGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				//selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'SiteStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:26
					},{
						text : "站点名称",
						dataIndex : "siteName",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "站点编号",
						dataIndex : "siteCode",
						width : 70,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "站点描述",
						dataIndex : "siteInfo",
						width : 35,
						editor : {
							xtype : "textfield",
		                    allowBlank: false
			            }
					}
		            ],
			listeners: { 
			},					
			initComponent : function() {
				// 可编辑插件
				this.callParent(arguments);
			},
			store : "SiteStore"
});