
Ext.define("app.view.FeePublishCensusGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.publishGrid",
			id : "publishGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
//			selModel : {
//				selType : "checkboxmodel"
//			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'FeePublishCensusStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"日期",
						dataIndex:"statdate",
						width:70
					},{
						text : "状态",
						dataIndex : "state",
						width : 60,
						editor : {
			                xtype : 'combobox',
			                editable : false,
			                store : [['2', '已发布'],
	                         ['1', '未发布']
			            ]},
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
					        if (v==1) {
					        	v = "<a style='color:red'>未发布</font>";
					        }else{
					        	v = "已发布";
					        }
					        return v;
					    }
					},{
		                xtype: 'actioncolumn',
		                width: 25,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
		                    tooltip: '发布',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var statdate = record.get('statdate');
		                        this.updatePublishData(grid,statdate);
		                    }
		                }]
		            }],
			listeners: { 
				beforeedit: function (editor, e, eOpts) {
//		            if(e.field == 'createTime'){
//		            	return false;
//		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
//		            	return false;
//		            }else{
//		            	return true;
//		            }
					return true;
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "FeePublishCensusStore"
});

var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"正在加载中..."});  

//根据id变更状态
function updatePublishData(grid,statdate){
	var store = grid.getStore();
	
	Ext.Ajax.request({
		url : "updatePublishData",
		params : {
			tdate : statdate
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
				store.load();
				// 只刷新这一行
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败', '');
			}
		}
	});
}
