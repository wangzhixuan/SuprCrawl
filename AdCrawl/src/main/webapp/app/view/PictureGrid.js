function showErrorMsg(msg){
	Ext.example.msg('', msg, '');
}

// 获取相册菜单
var getAllPictureMenuStore = Ext.create('Ext.data.Store', {
     model: 'app.model.MenuModel',
     proxy: {
         type: 'ajax',
         url: 'getAllPictureMenu',
         reader: {
             type: 'json',
             root: 'data'
         }
     },
     autoLoad: false
 });


var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'menuId',
		xtype : 'combo',
		fieldLabel : '菜单',
		labelWidth : 30,
		store : getAllPictureMenuStore,
		displayField : 'menuName',
		valueField : 'id',
		name : 'menuId',
		typeAhead:false,
		editable:false,
		triggerAction : 'all',
		emptyText : '===请选择===',
		anchor : '95%',
		width : 170
	},{
		id:'title',
		xtype : 'textfield',
		name : 'title',
		fieldLabel : '相册标题',
		labelWidth : 60,
		width : 220,
		emptyText : '===请填写相册标题===',
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("title").setValue("");
			Ext.getCmp("menuId").setValue("");
			search();
		}
	}]
});

function search(){
	var title = Ext.getCmp("title").getValue();
	var menuId = Ext.getCmp("menuId").getValue();
	// 获取grid的store
	var picStore = Ext.getCmp("pictureGrid").getStore();
	picStore.load({
		params : {
			title : encodeURIComponent(title),
			menuId:menuId
        }
	});
}

Ext.define("app.view.PictureGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.pictureGrid",
			id : "pictureGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				//selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'PictureStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:10
					},{
						text : "分类",
						dataIndex : "menuName",
						width : 35,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
		                xtype: 'actioncolumn',
		                width: 10,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/add.png',
		                    tooltip: '处理',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.loadAlbumById(grid,id);
		                    },
		                    getClass: function(v, meta, record) {
		                    	return '';
		                    }
		                }
		                ]
		            }
					],
			listeners: { 
				itemdblclick:function(grid,row){
		            var id = row.data.id;
		            loadAlbumById(grid,id);
		        }
			},					
			initComponent : function() {
				this.callParent(arguments);
			},
			store : "PictureStore"
});


function loadAlbumById(grid,id){
	var store = grid.getStore();
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '相册详情',
        width: 610,
        height: 483,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'getPictureInfoById?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		//myMask.show();  
        	}
       },
        buttonAlign: "center",  
        buttons: [{
            text: '上一个',
            handler: function() {
            	// 获取上一个
            	var newId = getId(1,id);
            	if(newId == null || newId == ''){
            		Ext.example.msg('', '无数据', '');
            	}else{
	            	win.getLoader().load({  
	                    url : './getPictureInfoById?id='+newId
	                });
	                id = newId;
            	}
            }
        },{
            text: '下一个',
            handler: function() {
            	// 获取下一个
            	var newId = getId(2,id);
            	if(newId == null || newId == '' || newId == 'null'){
            		Ext.example.msg('', '无数据', '');
            	}else{
	            	win.getLoader().load({  
	                    url : './getPictureInfoById?id='+newId
	                });
	                id = newId;
            	}
            }
        }]
    });
    
    win.show();
}


function getId(orient,value){
	var id = 0;
	
	Ext.Ajax.request({
			url : "getPictureId",
			params : {
				id:value,
				orient:orient
			},
			type : "POST",
			timeout : 4000,
			async:false,
			success : function(response, opts) {
				var obj = eval('('+response.responseText+')');
				id = obj.data;
			}
	});
	
	return id;
}
