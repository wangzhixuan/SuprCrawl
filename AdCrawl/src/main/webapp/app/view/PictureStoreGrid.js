function showErrorMsg(msg){
	Ext.example.msg('', msg, '');
}

//获取站点列表
var getAllSiteStore = Ext.create('Ext.data.Store', {
     model: 'app.model.SiteModel',
     proxy: {
         type: 'ajax',
         url: 'getAllSiteList?type=2',
         reader: {
             type: 'json',
             root: 'data'
         }
     },
     autoLoad: false
 });

var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'siteId',
		xtype : 'combo',
		fieldLabel : '站点',
		labelWidth : 30,
		store : getAllSiteStore,
		displayField : 'siteName',
		valueField : 'id',
		name : 'siteId',
		typeAhead:false,
		editable:false,
		triggerAction : 'all',
		emptyText : '===请选择===',
		anchor : '95%',
		width : 170
	},{
		id:'statue',
		xtype : 'combo',
		fieldLabel : '是否同步',
		labelWidth : 60,
		store : [	['-1', '全部'],
					['0', '未同步'],
	                ['1', '已同步'],
//	                ['2', '不可用'],
	                ['3', '同步中']],
		name : 'statue',
		valueField : 'id',
		typeAhead:false,
		editable:false,
		value:'-1',
		triggerAction : 'all',
		emptyText : '==请选择==',
		anchor : '95%',
		width : 150
	},{
		id:'title',
		xtype : 'textfield',
		name : 'title',
		fieldLabel : '相册标题',
		labelWidth : 60,
		width : 220,
		emptyText : '===请填写相册标题===',
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("siteId").setValue("");
			Ext.getCmp("title").setValue("");
			Ext.getCmp("statue").setValue("-1");
			search();
		}
	},{
		text : '批量同步',
		handler : function() {
			batchSync();
		}
	}]
});


// 批量同步相册
function batchSync(){
	var grid = Ext.getCmp("pictureStoreGrid");
	var store = grid.getStore();
	var records = grid.getSelectionModel().getSelection();
	
	var data = [];
	var model = store.model;
	Ext.Array.each(records, function(model) {
		if(model.get("id")!='0' && model.get("statue")=='0'){
			data.push(model.get("id"));
		}/*else{
			store.remove(model);
		}*/
	});
	if (data.length > 0) {
		var win = Ext.create('Ext.window.Window', {
	    	autoShow: false,
	        title: '批量同步',
	        width: 280,
	        height: 150,
	        layout: 'fit',
	        plain:true,
	        modal: 'true',  // 模态
	        collapsible: true, //允许缩放条  
	        closeAction: 'destroy',  
	        autoScroll: true,
	        maximizable :true,
	        loader: {
	            url:'getPicMenuList',
	            scripts: true,
	            autoLoad: true
	        },
	        listeners:{
	        	'beforerender':function(){
	        		//myMask.show();  
	        	}
	       },
	        buttonAlign: "center",  
	        buttons: [{
	            text: '同步',
	            handler: function() {
	            	var ids
	            	Ext.Ajax.request({
						url : "batchSyncAlbumStore",
						params : {
							ids : "" + data.join(",") + "",
							menuId : $("#menuId").val()
						},
						type : "POST",
						timeout : 8000,
						success : function(response, opts) {
							var result = eval('('+response.responseText+')');
							if(result.resultCode == 'success'){
								search();
								win.close();
								Ext.example.msg('', '批量同步成功', '');
							}else{
								Ext.example.msg('', '批量同步失败', '');
							}
						}
					});
	            }
	        }]
	    });
	    win.show();
	} else {
		Ext.example.msg('', '请选择未同步数据', '');
	}
}

function search(){
	var title = Ext.getCmp("title").getValue();
	var siteId = Ext.getCmp("siteId").getValue();
	var statue = Ext.getCmp("statue").getValue();
	// 获取grid的store
	var picStore = Ext.getCmp("pictureStoreGrid").getStore();
	picStore.load({
		params : {
			title : encodeURIComponent(title),
			siteId:siteId,
			statue:statue
        }
	});
}

Ext.define("app.view.PictureStoreGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.pictureStoreGrid",
			id : "pictureStoreGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'PictureStoreStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:10
					},{
						text : "封面",
						dataIndex : "albumUrl",
						width : 85,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                },
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
							var resultStr = "";
							var strs= new Array(); //定义一数组 
							strs = v.split(";"); //字符分割 
							for (i=0;i<strs.length;i++){
								if(strs[i] != null && strs[i] != ''){
									resultStr += "<img src='"+strs[i]+"' style='width:120px'>"
								}
							}
							
							return resultStr;
					    }
					},{
						text : "状态",
						dataIndex : "statue",
						width : 16,
						editor : {
			                xtype : 'combobox',
			                editable : false,
			                store : [['0', '未同步'],
	                         ['1', '已同步'],
	                         ['2', '不可用']
			            ]},
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
					        if(v == '0'){
					        	return "未同步";
					        }else if(v == '1'){
					        	return "<font style='color:red'>已同步</font>";
					        }else if(v == '2'){
					        	return "不可用";
					        }else if(v == '3'){
					        	return "<font style='color:red'>同步队列中</font>";
					        }
					    }
					},{
						text : "抓取时间",
						dataIndex : "createTime",
						width : 30,
						editor : {
							xtype : "textfield",
		                    allowBlank: false
			            }
					},{
		                xtype: 'actioncolumn',
		                width: 10,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/add.png',
		                    tooltip: '处理',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.loadAlbumById(grid,id);
		                    },
		                    getClass: function(v, meta, record) {
		                    	return '';
		                    }
		                }
//		                ,{
//		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/change.png',
//		                    tooltip: '状态切换',
//		                    scope: this,
//		                    handler: function(grid, rowIndex, colIndex) {
//		                        var record = grid.getStore().getAt(rowIndex);
//		                        var id = record.get('id');
//		                        var statue = record.get('statue');
//		                        this.deleteAppById(grid,id,statue);
//		                    },
//		                    getClass: function(v, meta, record) {
//		                    	return '';
//		                    }
//		                },{
//		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/special.png',
//		                    tooltip: '下载',
//		                    scope: this,
//		                    handler: function(grid, rowIndex, colIndex) {
//		                        var record = grid.getStore().getAt(rowIndex);
//		                        var sdkUrl = record.get('sdkUrl');
//		                        window.open(sdkUrl);
//		                    },
//		                    getClass: function(v, meta, record) {
//		                    	return '';
//		                    }
//		                }
		                ]
		            }],
			listeners: { 
				itemdblclick:function(grid,row){
		            var id = row.data.id;
		            loadAlbumById(grid,id);
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "PictureStoreStore"
});

// 根据id删除APP
function deleteAppById(grid,id,statue){
	
	if(statue != 0){
		statue = 0;
	} else {
		statue = 1;
	}	
	
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "deleteAppById",
		params : {
			id : id,
			statue : statue
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
				store.load();
				Ext.example.msg('', '切换成功', '');
			}else{
				Ext.example.msg('', '切换失败', '');
			}
		}
	});
}

function loadAlbumById(grid,id){
	var store = grid.getStore();
	var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '相册详情',
        width: 610,
        height: 483,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'getPictureStoreInfoById?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		//myMask.show();  
        	}
       },
        buttonAlign: "center",  
        buttons: [{
            text: '上一个',
            handler: function() {
            	// 获取上一个
            	var newId = getId(1,id);
            	if(newId == null || newId == ''){
            		Ext.example.msg('', '无数据', '');
            	}else{
	            	win.getLoader().load({  
	                    url : './getPictureStoreInfoById?id='+newId
	                });
	                id = newId;
            	}
            }
        },{
            text: '删除',
            handler: function() {
            	Ext.Ajax.request({
					url : "deletePictureStoreById",
					params : {
						id : id
					},
					type : "POST",
					timeout : 4000,
					success : function(response, opts) {
						var result = eval('('+response.responseText+')');
						if(result.resultCode == 'success'){
							search();
							Ext.example.msg('', '删除成功', '');
						}else{
							Ext.example.msg('', '删除失败', '');
						}
					}
				});
				
            	// 删除后获取上一个
            	var newId = getId(1,id);
            	if(newId == null || newId == ''){
            		Ext.example.msg('', '无数据', '');
            	}else{
	            	win.getLoader().load({  
	                    url : './getPictureStoreInfoById?id='+newId
	                });
	                id = newId;
            	}
            }
        },{
            text: '下一个',
            handler: function() {
            	// 获取下一个
            	var newId = getId(2,id);
            	if(newId == null || newId == '' || newId == 'null'){
            		Ext.example.msg('', '无数据', '');
            	}else{
	            	win.getLoader().load({  
	                    url : './getPictureStoreInfoById?id='+newId
	                });
	                id = newId;
            	}
            }
        }]
    });
    
    win.show();
}


function getId(orient,value){
	var id = 0;
	var title = encodeURIComponent(Ext.getCmp("title").getValue());
	var siteId = Ext.getCmp("siteId").getValue();
	
	Ext.Ajax.request({
			url : "getPictureStoreId",
			params : {
				id:value,
				title:title,
				siteId:siteId,
				orient:orient
			},
			type : "POST",
			timeout : 4000,
			async:false,
			success : function(response, opts) {
				var obj = eval('('+response.responseText+')');
				id = obj.data;
			}
	});
	
	return id;
}

