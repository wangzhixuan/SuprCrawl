var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
			/*xtype : 'button',
			text : '删除',
			id : 'delete'
		}, {*/
		id:'appName',
		xtype : 'textfield',
		name : 'appName',
		fieldLabel : '应用名称',
		labelWidth : 60,
		width : 205,
		emptyText : '===请填写应用名称===',
		listeners:{
			'blur':function(){
				search();
			}
		}
	},{
		/*xtype : 'button',
		text : '删除',
		id : 'delete'
	}, {*/
		id:'cpName',
		xtype : 'textfield',
		name : 'cpName',
		fieldLabel : 'CP名称',
		labelWidth : 60,
		width : 205,
		emptyText : '===请填写CP名称===',
		listeners:{
			'blur':function(){
				search();
			}
		}
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("appName").setValue("");
			Ext.getCmp("cpName").setValue("");
			search();
		}
	}]
});

function search(){
	var appName = Ext.getCmp("appName").getValue();
	var cpName = Ext.getCmp("cpName").getValue();
	
	// 获取grid的store
	var cpStore = Ext.getCmp("rateGrid").getStore();
	cpStore.load({
		params : {
			appName : encodeURIComponent(appName),
			cpName : encodeURIComponent(cpName)
        }
	});
}

Ext.define("app.view.RateGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.rateGrid",
			id : "rateGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
			selModel : {
				//selType : "checkboxmodel"
			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'RateStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:20
					},{
						text : "应用名称",
						dataIndex : "appName",
						width : 100,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "CP名称",
						dataIndex : "cpName",
						width : 100,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "转化率",
						dataIndex : "rate",
						width : 100,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
		                xtype: 'actioncolumn',
		                width: 25,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
		                    tooltip: '编辑应用',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.updateAppById(grid,id);
		                    }
		                }]
		            }],
			listeners: { 
				beforeedit: function (editor, e, eOpts) {
		            if(e.field == 'createTime'){
		            	return false;
		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
		            	return false;
		            }else{
		            	return true;
		            }
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "RateStore"
});

function updateAppById(grid,id){
	if(id == null){
		Ext.example.msg('', '请选择一个应用', '');
	} else {
		var store = grid.getStore();
		 var win = Ext.create('Ext.window.Window', {
		    	autoShow: false,
		        title: '修改转化率',
		        width: 291,
		        height: 154,
		        minWidth: 200,
		        minHeight: 100,
		        layout: 'fit',
		        plain:true,
		        modal: 'true',  // 模态
		        collapsible: true, //允许缩放条  
		        closeAction: 'destroy',  
		        autoScroll: true,
		        maximizable :true,
		        loader: {
		            url:'updateBaseRateInfo?id='+id,
		            scripts: true,
		            autoLoad: true
		        },
		        listeners:{
		        	'beforerender':function(){
		        		//myMask.show();  
		        	}
		       },
		        buttonAlign: "center",  
		        buttons: [{
		            text: '保存',
		            handler: function() {
		            	// 保存前重新生成dimValue隐藏域的值
		            	//auto();
		            	if(validateTran()){
		            		var dataParm = decodeURIComponent($("#update_rate_form").serialize(),true);
			            	dataParm = encodeURI(encodeURI(dataParm));
			            	$.ajax({
			        			url : './updateAppRateInfo',
			        			data: dataParm,
			        			type : "post",
			        			dataType : "json",
			        			success : function(data) {
			        				if (data.resultCode == 'error') {
			        					return;
			        				} else if (data.resultCode == 'success') {
			        					// 刷新grid
			        					store.load();
			        					// 弹出新增成功消息框 
			        					Ext.example.msg('', '操作成功', '');
			        					// 关闭窗口
			        					win.close();
			        				}
			        			}
			        		});
		            	}
		            }
		        },{
		            text: '取消',
		            handler: function() {
		            	// 关闭窗口
		            	win.close();
		            }
		        }]
		    });
		    
		    win.show();
	}
}