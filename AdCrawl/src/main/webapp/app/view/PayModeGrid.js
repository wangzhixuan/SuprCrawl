var tbar = Ext.create('Ext.toolbar.Toolbar', {
	dock : 'top',
	layout: {  
        overflowHandler: 'Menu'  
    },  
	items : [{
		id:'payName',
		xtype : 'textfield',
		name : 'payName',
		fieldLabel : '支付名称',
		labelWidth : 60,
		width : 220,
		emptyText : '===请填写应用名称===',
		listeners:{
			'blur':function(){
				search();
			}
		}
	},{
		id:'statue',
		xtype : 'combo',
		name : 'statue',
		fieldLabel : '支付状态',
		labelWidth : 60,
		editable:false,
		width : 190,
		emptyText : '===请选择===',
		store : [['0', '正常'],
                 ['1', '关闭']],
        listeners:{
          	'change': function(val){
          		search(); 	
          	}
        }
	},{
		text : '查询',
		handler : function() {
			search();
		}
	},{
		text : '重置',
		handler : function() {
			Ext.getCmp("payName").setValue("");
			Ext.getCmp("statue").setValue("");
			search();
		}
	},{
		text : '添加',
		handler : function() {
			addPayMode();
		}
	}]
});

function search(){
	var payName = Ext.getCmp("payName").getValue();
	var statue = Ext.getCmp("statue").getValue();
	// 获取grid的store
	var payModeStore = Ext.getCmp("payModeGrid").getStore();
	payModeStore.load({
		params : {
			payName : encodeURIComponent(payName),
			statue:statue
        }
	});
}

Ext.define("app.view.PayModeGrid", {
			extend : "Ext.grid.Panel",
			xtype: 'cell-editing',
			alias : "widget.payModeGrid",
			id : "payModeGrid",
			width:700,
			height:350,
			viewConfig:{
		         loadMask:false  // grid去掉加载中的提示
		    },
//			selModel : {
//				selType : "checkboxmodel"
//			},
			border : 0,
			multiSelect : true,
			frame : false, // 不要边框
			tbar : tbar,
			bbar : {
				xtype : 'pagingtoolbar',
				store : 'PayModeStore',
				dock : 'bottom',
				displayInfo : true
			},
			viewConfig:{  
		        enableTextSelection:true  // 文本可选择
		    },
			enableKeyNav : true,
			columnLines : true,
			forceFit :true, // 宽度自适配
			columns : [{
						text:"序号",
						dataIndex:"id",
						width:20
					},{
						text : "支付方式",
						dataIndex : "payName",
						width : 65,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "支付方式编码",
						dataIndex : "payCode",
						width : 65,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "用户名",
						dataIndex : "userName",
						width : 75,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "回调URL",
						dataIndex : "callBackUrl",
						width : 170,
						sortable : false,
		                editor: {
		                	xtype : "textfield",
		                    allowBlank: false
		                }
					},{
						text : "状态",
						dataIndex : "statue",
						width : 80,
						editor : {
			                xtype : 'combobox',
			                editable : false
			            },
						renderer : function(v, p, record, rowIndex, colIndex) { // 值自定义渲染
							if (v==0) {
					        	v = "<font style='color:red'>开启</font>";
					        }else if (v==1) {
					        	v = "<font style='color:gray'>关闭</font>";
					        }
					        return v;
					    }
					},{
		                xtype: 'actioncolumn',
		                width: 45,
		                tdCls: 'action',  
		                text: '操作',  
		                sortable: false,
		                menuDisabled: true,
		                items: [{
		                	 icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/edit.png',
		                    tooltip: '编辑',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                    	var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.editPayMode(grid,id);
		                    },
		                    getClass: function(v, meta, record) {
//		                    	var payStatue = record.get('statue');
//		                        if (payStatue == '1') {
//		                              return 'emptyCss';
//		                        }else{
//		                              return '';
//		                        }
		                    }
		                },{
		                	 icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/change.png',
		                    tooltip: '状态切换',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                        var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        var payStatue = record.get('statue');
		                        this.updatePayModeStatueById(grid,id,payStatue,record);
		                    },
		                    getClass: function(v, meta, record) {
//		                    	var payStatue = record.get('statue');
//		                        if (payStatue == '1') {
//		                              return 'emptyCss';
//		                        }else{
//		                              return '';
//		                        }
		                    }
		                },{
		                	 icon: 'js/ext-4.2/resources/ext-theme-neptune/images/icon/close.png',
		                    tooltip: '删除',
		                    scope: this,
		                    handler: function(grid, rowIndex, colIndex) {
		                        var record = grid.getStore().getAt(rowIndex);
		                        var id = record.get('id');
		                        this.deletePayModeById(grid,id,record);
		                    },
		                    getClass: function(v, meta, record) {
		                    }
		                }]
		            }],
			listeners: { 
				beforeedit: function (editor, e, eOpts) {
//		            if(e.field == 'createTime'){
//		            	return false;
//		            }else if(e.record.raw.username == 'admin' && (e.field == 'userType' || e.field == 'username')){
//		            	return false;
//		            }else{
//		            	return true;
//		            }
					return true;
		        }
			},					
			initComponent : function() {
				// 可编辑插件
//				this.editing = Ext.create("Ext.grid.plugin.CellEditing");
//				this.plugins = [this.editing];
				this.callParent(arguments);
			},
			store : "PayModeStore"
});

var myMask = new Ext.LoadMask(Ext.getBody(), {msg:"正在加载中..."});  

//根据id变更状态
function updatePayModeStatueById(grid,id,payStatue,record){
	var store = grid.getStore();
	if(payStatue == 0){
		payStatue = 1;
	}else{
		payStatue = 0;
	}
	
	Ext.Ajax.request({
		url : "updatePayModeStatueById",
		params : {
			id : id,
			statue : payStatue
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				// 刷新grid
//				store.load();
				// 只刷新这一行
				record.set("statue",payStatue);
				record.commit();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败', '');
			}
		}
	});
}

//根据ID删除
function deletePayModeById(grid,id,record){
	var store = grid.getStore();
	Ext.Ajax.request({
		url : "deletePayModeById",
		params : {
			id : id
		},
		type : "POST",
		timeout : 4000,
		success : function(response, opts) {
			var result = eval('('+response.responseText+')');
			if(result.resultCode == 'success'){
				store.load();
				Ext.example.msg('', '操作成功', '');
			}else{
				Ext.example.msg('', '操作失败<br/>'+result.errorInfo, '');
			}
		}
	});
}


//编辑支付方式
function editPayMode(grid,id){
	// 获取grid的store
	var payModeStore = grid.getStore();
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '编辑支付方式',
        width: 450,
        height: 480,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'./editPayModel?id='+id,
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		myMask.show();  
        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	if(validate()){
	            	var dataParm = decodeURIComponent($("#add_pay_mode_form").serialize(),true);
	            	dataParm = encodeURI(encodeURI(dataParm));
	            	$.ajax({
	        			url : './savePayMode?data='+Math.random(),
	        			data: dataParm, 
	        			type : "post",
	        			dataType : "json",
	        			success : function(data) {
	        				if (data.resultCode == 'error') {
	        					return;
	        				} else if (data.resultCode == 'success') {
	        					// 弹出新增成功消息框 
	        					Ext.example.msg('', '操作成功', '');
	        					// 关闭窗口
	        					win.close();
	        					search();
	        				}
	        			}
	        		});
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}

//添加支付方式
function addPayMode(){
	// 获取grid的store
	var payModeStore = Ext.getCmp("payModeGrid").getStore();
	
    var win = Ext.create('Ext.window.Window', {
    	autoShow: false,
        title: '新增支付方式',
        width: 450,
        height: 480,
        minWidth: 300,
        minHeight: 200,
        layout: 'fit',
        plain:true,
        modal: 'true',  // 模态
        collapsible: true, //允许缩放条  
        closeAction: 'destroy',  
        autoScroll: true,
        maximizable :true,
        loader: {
            url:'addPayModel',
            scripts: true,
            autoLoad: true
        },
        listeners:{
        	'beforerender':function(){
        		myMask.show();  
        	}
        },
        buttonAlign: "center",  
        buttons: [{
            text: '保存',
            handler: function() {
            	if(validate()){
	            	var dataParm = decodeURIComponent($("#add_pay_mode_form").serialize(),true);
	            	dataParm = encodeURI(encodeURI(dataParm));
	            	$.ajax({
	        			url : './savePayMode?data='+Math.random(),
	        			data: dataParm, 
	        			type : "post",
	        			dataType : "json",
	        			success : function(data) {
	        				if (data.resultCode == 'error') {
	        					return;
	        				} else if (data.resultCode == 'success') {
	        					// 弹出新增成功消息框 
	        					Ext.example.msg('', '操作成功', '');
	        					// 关闭窗口
	        					win.close();
	        					search();
	        				}
	        			}
	        		});	
            	}
            }
        },{
            text: '取消',
            handler: function() {
            	// 关闭窗口
            	win.close();
            }
        }]
    });
    
    win.show();
}