Ext.define("app.model.ChannelModel",{
	extend:"Ext.data.Model",
	fields:[
	    {name:'id',type:'int',sortable:true},
	    {name:'channelName',type:'string',sortable:true},
	    {name:'channelCode',type:'string',sortable:true}
	]
});