Ext.define("app.model.PictureModel",{
	extend:"Ext.data.Model",
	fields:[
	    {name:'id',type:'int',sortable:true},
	    {name:'title',type:'string',sortable:true},
	    {name:'menuName',type:'string',sortable:true}
	]
});