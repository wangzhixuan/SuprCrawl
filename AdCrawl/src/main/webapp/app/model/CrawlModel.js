Ext.define("app.model.CrawlModel",{
	extend:"Ext.data.Model",
	fields:[
	    {name:'id',type:'int',sortable:true},
	    {name:'siteId',type:'int',sortable:true},
	    {name:'siteCategoryId',type:'int',sortable:true},
	    {name:'type',type:'int',sortable:true},
	    {name:'listUrl',type:'string',sortable:true},
	    {name:'start',type:'int',sortable:true},
	    {name:'end',type:'int',sortable:true},
	    {name:'infoUrlXpath',type:'string',sortable:true},
	    {name:'infoUrlRegular',type:'string',sortable:true},
	    {name:'infoUrlPre',type:'string',sortable:true},
	    {name:'infoContentXpath',type:'string',sortable:true},
	    {name:'infoPicXpath',type:'string',sortable:true},
	    {name:'repeatFlagXpath',type:'string',sortable:true},
	    {name:'repeatMaxTimes',type:'int',sortable:true},
	    {name:'crawlTime',type:'int',sortable:true},
	    {name:'sleepTime',type:'int',sortable:true},
	    {name:'statue',type:'int',sortable:true},
	    {name:'lastCrawlTime',type:'string',sortable:true},
	    {name:'siteName',type:'string',sortable:true},
	    {name:'categoryName',type:'string',sortable:true}
	]
});