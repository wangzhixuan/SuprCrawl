package com.cmge.ad.mapper;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.AlbumStore;
import com.cmge.ad.model.ArticleStore;
import com.cmge.ad.model.Crawl;
import com.cmge.ad.model.LockPaint;
import com.cmge.ad.model.PaintReq;
import com.cmge.ad.model.PictureStore;


public interface CrawlMapper {

	void addCrawl(Crawl crawl);

	Crawl scanCrawlTask();

	void startCralTask(Crawl crawl);

	void stopCralTask(Crawl crawl);

	void addArticleStore(ArticleStore articleStore);

	int isExistArticleUniqueId(String uniqueId);

	int getCrawlCount(HashMap<String, Object> paramMap);

	List<Crawl> getCrawlList(HashMap<String, Object> paramMap);

	Crawl scanCrawlTaskById(String id);

	void updateCrawlInfo(Crawl crawl);

	int isExistAlbumUniqueId(String uniqueId);

	void addAlbumStore(AlbumStore albumStore);

	void addPictureStore(PictureStore ps);

	void addAlbumCrawlInfo(Crawl crawl);

	List<Crawl> getAutoCrawlList();

	void savePaint(LockPaint paint);

	List<LockPaint> getPaintList(PaintReq getReq);

}
