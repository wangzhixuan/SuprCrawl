package com.cmge.ad.mapper;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.Album;
import com.cmge.ad.model.AlbumStore;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Picture;


public interface PictureMapper {

	int getPictureStoreCount(HashMap<String, Object> paramMap);

	List<AlbumStore> getPictureStoreList(HashMap<String, Object> paramMap);

	AlbumStore getPictureStoreInfoById(String id);

	List<Picture> getPictureList(String id);

	String getPictureStoreId(AlbumStore album);

	void deletePictureStoreById(Integer id);

	void deletePictureStoreInfoById(Integer id);

	List<Menu> getPictureChildMenu();

	int existSyncPicture(Integer id);

	void syncPictureStore(HashMap<String, Object> param);

	void syncPictureStoreStatue(Integer id);

	List<AlbumStore> getSyncAlbumList();

	void deleteSyncAlbum(AlbumStore album);

	void updatePictureStoreStatue(AlbumStore album);

	List<Picture> getPictureListByAlbumId(Integer id);

	void addAlbum(AlbumStore album);

	void addPicture(Picture pic);

	int getPictureCount(HashMap<String, Object> paramMap);

	List<Album> getPictureList(HashMap<String, Object> paramMap);

	List<Menu> getAllPictureMenu();

	List<Picture> getPictureStoreInfoList(String id);

	Album getPictureInfoById(String id);

	List<Picture> getPictureListById(String id);

	String getPictureId(Album album);
}
