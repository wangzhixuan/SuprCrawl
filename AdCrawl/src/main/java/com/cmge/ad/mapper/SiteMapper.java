package com.cmge.ad.mapper;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Site;
import com.cmge.ad.model.SiteCategory;


public interface SiteMapper {

	List<Site> getAllSiteList(int type);

	List<SiteCategory> getCategoryBySiteId(String siteId);

	int getSiteCount(HashMap<String, Object> paramMap);

	List<Site> getSiteList(HashMap<String, Object> paramMap);

	int getSiteCategoryCount(HashMap<String, Object> paramMap);

	List<SiteCategory> getSiteCategoryList(HashMap<String, Object> paramMap);

	int getMenuCount(HashMap<String, Object> paramMap);

	List<Menu> getMenuList(HashMap<String, Object> paramMap);

	List<Menu> getParentMenu();

	void addChildMenu(Menu menu);

	int existMenu(Menu menu);

	void deleteChildMenuById(Integer id);

	int existMenuRelation(Integer id);
	
	/**获取所有站点信息**/
	List<Site> getAllSiteInfoList();

	/**通过父类id获取子类栏目**/
	List<Menu> getMenuListByParentId(String parentId);
	
	/**根据站点id选择分类**/
	List<SiteCategory> getSiteCategoryListBySiteId(String siteId);
	
	/**获取所有子类栏目**/
	List<Menu> getChildrenMenuList();
}
