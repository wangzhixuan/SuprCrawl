package com.cmge.ad.spider.article.hunduanzi;

import java.util.List;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.cmge.ad.model.Article;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc	Json展示
 * @author	ljt
 * @time	2014-12-29 下午7:22:48
 */
public class JsonPipeline implements Pipeline{
	
	@Override
	public void process(ResultItems resultItems, Task task) {
		List<Article> articleList = (List<Article>)resultItems.get("articleList");
		if(null != articleList && articleList.size() > 0){
			for(Article article : articleList){
				System.out.println(JsonUtil.toJson(article));
			}
		}
	}
	
}
