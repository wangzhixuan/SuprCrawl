package com.cmge.ad.spider.article.lengxiaohua;

import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Article;
import com.cmge.ad.spider.pipeline.MysqlArticlePipeline;

/**
 * @desc	冷笑话
 * 			http://m.lengxiaohua.com/text?page_num=1
 * @author	ljt
 * @time	2014-12-29 上午11:16:05
 */
public class MLengXiaoHuaPageProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "text?page_num=\\w+";
    
    // 列表最大值
    private int max = 3939;
    
    @Override
    public void process(Page page) {
    	// 检索当前页面所有段子
    	List<Selectable> cList = page.getHtml().xpath("//div[@class='joke_box joke_list_page']//a[@class='joke_tx p_joke_tx']/text()").nodes();
    	List<Article> articleList = new ArrayList<Article>();
		if(null != cList && cList.size() > 0){
			for(Selectable str : cList){
				Article article = new Article();
				String content = str.get();
				article.setContent(content.trim());
				article.setSource("lengxiaohua_lengxiaohua");
				article.setType(1);
				article.setMinImageUrl("");
				article.setMaxImageUrl("");
				articleList.add(article);
    		}
			page.putField("articleList",articleList);
			page.putField("type", 1);
		}
		
		// 当前页
		String url = page.getUrl().get();
		int current = 1;
		try {
			current = Integer.parseInt(url.substring(url.lastIndexOf("=")+1,url.length()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("current is :"+current);
		
		if(current < max){
			page.addTargetRequest("http://m.lengxiaohua.com/text?page_num="+(current+1));
		}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new MLengXiaoHuaPageProcessor())
    					.addUrl("http://m.lengxiaohua.com/text?page_num=1")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
//    					.addPipeline(new JsonPipeline())
    					.addPipeline(new MysqlArticlePipeline())
    					.thread(1);
    	qsSpider.start();
    }
    
}
