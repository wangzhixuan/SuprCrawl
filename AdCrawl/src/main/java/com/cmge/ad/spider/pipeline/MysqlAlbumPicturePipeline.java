package com.cmge.ad.spider.pipeline;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.cmge.ad.model.Album;
import com.cmge.ad.model.Picture;
import com.cmge.ad.service.MainService;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc	抓取内容存放到数据库中
 * @author	ljt
 * @time	2015-1-4 上午10:48:30
 */
@Component
public class MysqlAlbumPicturePipeline implements Pipeline{
	
	@Autowired
	private MainService mainService;
	
	static{
//		JdbcUtils.getConnection();
	}
	
	@Override
	public void process(ResultItems resultItems, Task task) {
		Integer type = (Integer)resultItems.get("type");
		if(type != null && type.intValue() == 1){
			// 图片
			Picture pic = (Picture)resultItems.get("picture");
			if(null != pic){
				System.out.println(JsonUtil.toJson(pic));
				String sql = "insert into t_picture_src (albumId, minImageUrl,maxImageUrl,description,source,createTime) " +
						"values (?,?,?,?,?,now())";
				try {
					List<Object> params = new ArrayList<Object>();
					params.add(pic.getAlbumId());
					params.add(pic.getMinImageUrl());
					params.add(pic.getMaxImageUrl());
					params.add(pic.getDesc());
					params.add(pic.getSource());
					boolean flag = JdbcUtils.updateByPreparedStatement(sql, params);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		if(type != null && type.intValue() == 2){
			// 相册
			Album album = (Album)resultItems.get("album");
			if(null != album){
				System.out.println(JsonUtil.toJson(album));
				String al_sql = "insert into t_album (albumName, albumUrl,title,createTime) " +
						"values (?,?,?,now())";
				try {
					List<Object> params = new ArrayList<Object>();
					params.add(album.getAlbumName());
					params.add(album.getAlbumUrl());
					params.add(album.getTitle());
					boolean flag = JdbcUtils.updateByPreparedStatement(al_sql, params);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				List<Picture> picList = album.getPictureList();
				for(Picture pic : picList){
					String pic_sql = "insert into t_picture_src (albumId, minImageUrl,maxImageUrl,description,source,createTime) " +
							"values (?,?,?,?,?,now())";
					try {
						List<Object> params = new ArrayList<Object>();
						int albumId = (Integer)JdbcUtils.findSimpleResult("select max(id) as Id from t_album", null).get("Id");
						params.add(albumId);
						params.add(pic.getMinImageUrl());
						params.add(pic.getMaxImageUrl());
						params.add(pic.getDesc());
						params.add(pic.getSource());
						boolean flag = JdbcUtils.updateByPreparedStatement(pic_sql, params);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		
		if (type != null && type.intValue() == 3) {
			// 图片list
			List<Picture> picList = (List<Picture>) resultItems.get("picList");
			if (null != picList && picList.size() > 0) {
				System.out.println(JsonUtil.toJson(picList));
				String sql = "insert into t_picture_src (albumId, minImageUrl,maxImageUrl,description,source,createTime) " + "values (?,?,?,?,?,now())";
				for(Picture pic : picList){
					try {
						List<Object> params = new ArrayList<Object>();
						params.add(pic.getAlbumId());
						params.add(pic.getMinImageUrl());
						params.add(pic.getMaxImageUrl());
						params.add(pic.getDesc());
						params.add(pic.getSource());
						JdbcUtils.updateByPreparedStatement(sql, params);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

		}
		
	}
	
}