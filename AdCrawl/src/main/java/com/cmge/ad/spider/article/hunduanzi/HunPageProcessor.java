package com.cmge.ad.spider.article.hunduanzi;

import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Article;
import com.cmge.ad.spider.pipeline.MysqlArticlePipeline;

/**
 * @desc	荤段子  已爬
 * 			http://hdz.gouleide.com/
 * @author	ljt
 * @time	2014-12-29 上午11:16:05
 */
public class HunPageProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "/list-0-\\w+";
    
    // 列表最大值
    private int max = 111;
    
    @Override
    public void process(Page page) {
    	// 检索当前页面所有段子
    	List<Selectable> cList = page.getHtml().xpath("//div[@class='dz']").nodes();
    	List<Article> articleList = new ArrayList<Article>();
		if(null != cList && cList.size() > 0){
			for(Selectable str : cList){
				Article article = new Article();
				String content = str.xpath("//div[@class='dz_content']/text()").get();
//				String ding = str.xpath("//div[@class='dz_dingcai']/a[@class='ding']/span/text()").get();
//				String cai = str.xpath("//div[@class='dz_dingcai']/a[@class='cai']/span/text()").get();
				article.setContent(content);
				article.setSource("hunduanzi_hunduanzi");
				article.setType(1);
				article.setMinImageUrl("");
				article.setMaxImageUrl("");
				articleList.add(article);
    		}
			page.putField("articleList",articleList);
			page.putField("type", 1);
		}
		
		// 当前页
		String url = page.getUrl().get();
		int current = 1;
		try {
			current = Integer.parseInt(url.substring(url.lastIndexOf("-")+1,url.lastIndexOf(".html")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("current is :"+current);
		
		if(current < max){
			page.addTargetRequest("http://hdz.gouleide.com/list-0-"+(current+1)+".html");
		}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new HunPageProcessor())
    					.addUrl("http://hdz.gouleide.com/list-0-5.html")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
//    					.addPipeline(new JsonPipeline())
    					.addPipeline(new MysqlArticlePipeline())
    					.thread(1);
    	qsSpider.start();
    }
}
