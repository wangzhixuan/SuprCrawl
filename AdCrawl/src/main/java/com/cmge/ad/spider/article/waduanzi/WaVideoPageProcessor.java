package com.cmge.ad.spider.article.waduanzi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Video;

/**
 * @desc	挖段子视频	
 * 			http://www.waduanzi.com/mobile
 * @author	ljt
 * @time	2014-12-29 上午11:16:05
 */
public class WaVideoPageProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "/mobile/video/page/\\w+";
    
    // 列表最大值
    private int max = 336;
    
    private boolean flag = true;
    
    @Override
    public void process(Page page) {
    	// 检索当前页面所有段子
    	List<Selectable> cList = page.getHtml().xpath("//div[@class='panel panel10 post-item post-box']").nodes();
    	List<Video> videoList = new ArrayList<Video>();
		if(null != cList && cList.size() > 0){
			for(Selectable str : cList){
				Video video = new Video();
				String author = str.xpath("//div[@class='post-author']/a/text()").get();
				String content = str.xpath("//div[@class='item-content']/text()").get();
				String ding = str.xpath("//div[@class='item-toolbar']//a[@class='upscore site-bg']/text()").get();
				String cai = str.xpath("//div[@class='item-toolbar']//a[@class='downscore site-bg']/text()").get();
				String commentCount = str.xpath("//div[@class='item-toolbar']//li[@class='fright']/a[@class='site-bg comment']/text()").get();
				String sb = str.xpath("//div[@class='item-detail']/div/script").get();
				String fileUrl = "";
				if(StringUtils.isEmpty(sb)){
					// 不是优酷的视频  暂时不抓取
					continue;
				}else{
					// 优酷的地址
					// 文件格式 http://player.youku.com/player.php/sid/XODU1MjMwNTAw/partnerid/1f2d57f9b5ea2ce9/v.swf
					fileUrl = sb.substring(sb.indexOf("-")+1,sb.indexOf("'", sb.indexOf("-")));
					fileUrl = "http://player.youku.com/player.php/sid/"+fileUrl+"/partnerid/1f2d57f9b5ea2ce9/v.swf";
				}
				
				video.setAuthor(author.trim());
				video.setCai(cai == null ? 0 : Integer.parseInt(cai));
				video.setDing(ding == null ? 0 : Integer.parseInt(ding));
//				if(StringUtils.isEmpty(commentCount) || commentCount.equals("吐槽")){
//					video.setCommentCount(0);
//				}else{
//					video.setCommentCount(Integer.parseInt(commentCount));
//				}
				video.setTitle(content.trim());
				video.setFileUrl(fileUrl);
				
				// 如果视频文件为空  则不需要保存
				if(!StringUtils.isEmpty(fileUrl)){
					videoList.add(video);
				}
    		}
			page.putField("videoList",videoList);
		}
		
		if(flag){
			for(int i = 2;i<max;i++){
				page.addTargetRequest("http://www.waduanzi.com/mobile/video/page/"+i);
			}
			flag = false;
		}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new WaVideoPageProcessor())
    					.addUrl("http://www.waduanzi.com/mobile/video/page/1")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
    					.addPipeline(new JsonVideoPipeline())
    					.thread(1);
    	qsSpider.start();
    }
}
