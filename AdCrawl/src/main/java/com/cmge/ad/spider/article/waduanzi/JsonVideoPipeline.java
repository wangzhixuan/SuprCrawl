package com.cmge.ad.spider.article.waduanzi;

import java.util.List;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.cmge.ad.model.Video;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc	Json展示
 * @author	ljt
 * @time	2014-12-29 下午7:22:48
 */
public class JsonVideoPipeline implements Pipeline{
	
	@Override
	public void process(ResultItems resultItems, Task task) {
		List<Video> videoList = (List<Video>)resultItems.get("videoList");
		if(null != videoList && videoList.size() > 0){
			for(Video video : videoList){
				System.out.println(JsonUtil.toJson(video));
			}
		}
	}
	
}
