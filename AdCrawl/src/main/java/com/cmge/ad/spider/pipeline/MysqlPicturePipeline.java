package com.cmge.ad.spider.pipeline;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import com.cmge.ad.model.Picture;
import com.cmge.ad.service.MainService;
import com.cmge.ad.util.JsonUtil;

/**
 * @desc	抓取内容存放到数据库中
 * @author	ljt
 * @time	2015-1-4 上午10:48:30
 */
@Component
public class MysqlPicturePipeline implements Pipeline{
	
	@Autowired
	private MainService mainService;
	
	static{
//		JdbcUtils.getConnection();
	}
	
	@Override
	public void process(ResultItems resultItems, Task task) {
		Picture pic = (Picture)resultItems.get("picture");
		if(null != pic){
			System.out.println(JsonUtil.toJson(pic));
			String sql = "insert into t_picture_src (albumId, minImageUrl,maxImageUrl,description,source,createTime) " +
					"values (?,?,?,?,?,now())";
			try {
				List<Object> params = new ArrayList<Object>();
				params.add(pic.getAlbumId());
				params.add(pic.getMinImageUrl());
				params.add(pic.getMaxImageUrl());
				params.add(pic.getDesc());
				params.add(pic.getSource());
				boolean flag = JdbcUtils.updateByPreparedStatement(sql, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}