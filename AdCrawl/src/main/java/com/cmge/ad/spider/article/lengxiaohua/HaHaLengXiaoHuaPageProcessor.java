package com.cmge.ad.spider.article.lengxiaohua;

import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;

import com.cmge.ad.model.Article;
import com.cmge.ad.spider.pipeline.MysqlArticlePipeline;

/**
 * @desc	哈哈冷笑话
 * 			http://www.haha365.com/zz_joke/index_1.htm
 * @author	ljt
 * @time	2014-12-29 上午11:16:05
 */
public class HaHaLengXiaoHuaPageProcessor implements PageProcessor {

    private Site site = Site.me().setRetryTimes(3).setSleepTime(100);

    public static final String URL_LIST = "jzw/\\w+";
    
    // 列表最大值
    private int max = 1258;
    
    @Override
    public void process(Page page) {
    	// 检索当前页面所有段子
    	List<Selectable> cList = page.getHtml().xpath("//div[@id='endtext']//text()").nodes();
    	List<Article> articleList = new ArrayList<Article>();
		if(null != cList && cList.size() > 0){
			for(Selectable str : cList){
				Article article = new Article();
				String content = str.get();
				article.setContent(content.trim());
				article.setSource("haha365_lengxiaohua");
				article.setType(1);
				article.setMinImageUrl("");
				article.setMaxImageUrl("");
				articleList.add(article);
    		}
			page.putField("articleList",articleList);
			page.putField("type", 1);
		}
		
		// 当前页
		String url = page.getUrl().get();
		int current = 1;
		try {
			current = Integer.parseInt(url.substring(url.lastIndexOf("_")+1,url.lastIndexOf(".htm")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("current is :"+current);
		
		if(current < max){
			page.addTargetRequest("http://www.haha365.com/zz_joke/index_"+(current+1)+".htm");
		}
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) throws Exception {
    	Spider qsSpider = Spider.create(new HaHaLengXiaoHuaPageProcessor())
    					.addUrl("http://www.haha365.com/zz_joke/index_1072.htm")
//    					.addPipeline(new RedisPipeline())
//    					.addPipeline(new JsonFilePipeline())
//    					.addPipeline(new JsonPipeline())
    					.addPipeline(new MysqlArticlePipeline())
    					.thread(1);
    	qsSpider.start();
    }
    
}
