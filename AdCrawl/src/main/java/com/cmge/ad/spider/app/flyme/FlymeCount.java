package com.cmge.ad.spider.app.flyme;

import java.util.List;

public class FlymeCount {
	
	private List<Flyme> value;

	public List<Flyme> getValue() {
		return value;
	}

	public void setValue(List<Flyme> value) {
		this.value = value;
	}
}
