package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

public class FeedBackReq extends BaseReqMsg {
	
	@ParamValidate(require=true,regex="^[\\S]{1,220}$")
	private String content;
	
	@ParamValidate(require=true,regex="^[\\S]{1,22}$")
	private String deviceId;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
