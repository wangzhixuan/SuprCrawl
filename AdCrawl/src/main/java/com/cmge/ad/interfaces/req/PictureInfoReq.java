package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

public class PictureInfoReq extends BaseReqMsg {
	
	@ParamValidate(require = true, regex = "^[\\d]{1,11}$")
	private Integer albumId;

	public Integer getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}
}
