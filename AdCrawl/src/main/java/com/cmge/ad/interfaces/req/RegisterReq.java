package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

/**
 * @desc	注册接口请求对象
 * @author	ljt
 * @time	2015-5-6 下午4:03:32
 */
public class RegisterReq extends BaseReqMsg {
	
	private Integer id;
	
	@ParamValidate(require = true, regex = "^[\\S]{1,32}$")
	// 唯一标识
	private String uuid;
	
	// 设备型号
	private String deviceName;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
}
