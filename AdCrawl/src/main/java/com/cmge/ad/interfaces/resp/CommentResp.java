package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class CommentResp extends ResponseMsg {

	public CommentResp() {
	}
	
	public CommentResp(int resultCode) {
		setResultCode(resultCode);
	}

}
