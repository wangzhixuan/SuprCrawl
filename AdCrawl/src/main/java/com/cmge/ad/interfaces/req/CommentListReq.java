package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;

/**
 * @desc	评论列表接口请求对象
 * @author	ljt
 * @time	2015-5-6 下午4:03:32
 */
public class CommentListReq extends BaseReqMsg {
	
	// 段子或图片的id
	private Integer id;
	
	// 类型 1:段子 2：图片
	private int type;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
