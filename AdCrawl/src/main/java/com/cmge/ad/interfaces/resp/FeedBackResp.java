package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class FeedBackResp extends ResponseMsg {

	public FeedBackResp() {
	}
	
	public FeedBackResp(int resultCode) {
		setResultCode(resultCode);
	}

}
