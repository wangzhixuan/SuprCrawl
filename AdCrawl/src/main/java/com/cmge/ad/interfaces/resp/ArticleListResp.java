package com.cmge.ad.interfaces.resp;

import java.util.List;

import com.cmge.ad.interfaces.vo.Article;
import com.cmge.ad.interfaces.vo.ResponseMsg;

public class ArticleListResp extends ResponseMsg {

	List<Article> articleList;
	
	public List<Article> getArticleList() {
		return articleList;
	}

	public void setArticleList(List<Article> articleList) {
		this.articleList = articleList;
	}

	public ArticleListResp() {
	}
	
	public ArticleListResp(int resultCode) {
		setResultCode(resultCode);
	}

}
