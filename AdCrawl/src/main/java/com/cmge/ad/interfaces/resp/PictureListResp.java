package com.cmge.ad.interfaces.resp;

import java.util.List;

import com.cmge.ad.interfaces.vo.ResponseMsg;
import com.cmge.ad.interfaces.vo.Album;

public class PictureListResp extends ResponseMsg {

	private List<Album> albumList;
	
	public List<Album> getAlbumList() {
		return albumList;
	}

	public void setAlbumList(List<Album> albumList) {
		this.albumList = albumList;
	}

	public PictureListResp() {
	}
	
	public PictureListResp(int resultCode) {
		setResultCode(resultCode);
	}

}
