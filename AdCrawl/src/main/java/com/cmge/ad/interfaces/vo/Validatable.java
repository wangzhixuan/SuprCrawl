package com.cmge.ad.interfaces.vo;

public interface Validatable {
	
	public ValidateResult validate() throws Exception;
	
}
