package com.cmge.ad.interfaces.vo;

import org.apache.log4j.Logger;

import com.cmge.ad.util.Constant;

public class ValidateResult {

	private static final Logger logger = Logger.getLogger(ValidateResult.class);
	
	private int validateResult;
	
	private String errorInfo;
	
	private String errorValue;

	public void setResult(int validateResult, String errorInfo, String errorValue) {
		this.validateResult = validateResult;
		this.errorInfo = errorInfo;
		this.errorValue = errorValue;
		if(validateResult != Constant.DEAL_SUCCESS){
			logger.info("validateResult is:" + validateResult + ",errorInfo is:" + errorInfo + ",errorValue is:" + errorValue);
		}
	}

	public int getValidateResult() {
		return validateResult;
	}

	public void setValidateResult(int validateResult) {
		this.validateResult = validateResult;
	}

	public String getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(String errorInfo) {
		this.errorInfo = errorInfo;
	}

	public String getErrorValue() {
		return errorValue;
	}

	public void setErrorValue(String errorValue) {
		this.errorValue = errorValue;
	}

}
