package com.cmge.ad.interfaces.resp;

import java.util.List;

import com.cmge.ad.interfaces.vo.Picture;
import com.cmge.ad.interfaces.vo.ResponseMsg;

public class PictureInfoResp extends ResponseMsg {

	private List<Picture> picList;
	
	public List<Picture> getPicList() {
		return picList;
	}

	public void setPicList(List<Picture> picList) {
		this.picList = picList;
	}

	public PictureInfoResp() {
	}
	
	public PictureInfoResp(int resultCode) {
		setResultCode(resultCode);
	}

}
