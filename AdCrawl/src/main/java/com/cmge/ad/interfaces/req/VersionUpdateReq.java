package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

public class VersionUpdateReq extends BaseReqMsg {
	
	@ParamValidate(require = true, regex = "^[\\d]{1,10}$")
	private int versionCode;

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}
}
