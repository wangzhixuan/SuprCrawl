package com.cmge.ad.interfaces.vo;

import java.io.Serializable;
import java.util.List;

public class ParentMenu implements Serializable{
	
	private Integer id;
	
	private String menuIcon;
	
	private String menuName;
	
	private String menuCode;
	
	private int menuType;
	
	private List<ChildMenu> menuList;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public int getMenuType() {
		return menuType;
	}

	public void setMenuType(int menuType) {
		this.menuType = menuType;
	}

	public List<ChildMenu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<ChildMenu> menuList) {
		this.menuList = menuList;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
}
