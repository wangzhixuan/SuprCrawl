package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class RegisterResp extends ResponseMsg {

	private String userId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public RegisterResp() {
	}
	
	public RegisterResp(int resultCode) {
		setResultCode(resultCode);
	}

}
