package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;

/**
 * @desc	评论接口请求对象
 * @author	ljt
 * @time	2015-5-6 下午4:03:32
 */
public class CommentReq extends BaseReqMsg {
	
	// 段子或图片的id
	private Integer id;
	
	// 类型 1:段子 2：图片
	private int type;
	
	// 评论内容
	private String comment;
	
	// 用户Id
	private Integer userId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
