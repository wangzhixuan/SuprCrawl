package com.cmge.ad.interfaces.vo;

import java.io.Serializable;


/**
 * @desc	图片
 * @author	ljt
 * @time	2014-12-30 下午7:48:30
 */
public class Picture implements Serializable{
	
	private Integer id;
	
	private String minImageUrl;
	
	private String maxImageUrl;
	
	private String locationUrl;
	
	// 描述
	private String description;
	
	private int zan;
	
	private int width;
	
	private int height;

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMinImageUrl() {
		return minImageUrl;
	}

	public void setMinImageUrl(String minImageUrl) {
		this.minImageUrl = minImageUrl;
	}

	public String getMaxImageUrl() {
		return maxImageUrl;
	}

	public void setMaxImageUrl(String maxImageUrl) {
		this.maxImageUrl = maxImageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}
}
