package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class ZanResp extends ResponseMsg {

	public ZanResp() {
	}
	
	public ZanResp(int resultCode) {
		setResultCode(resultCode);
	}

}
