package com.cmge.ad.interfaces.req;

import com.cmge.ad.interfaces.vo.BaseReqMsg;
import com.cmge.ad.interfaces.vo.ParamValidate;

public class ZanReq extends BaseReqMsg {
	
	@ParamValidate(require = true, regex = "^[\\d]{1,11}$")
	private Integer id;
	
	@ParamValidate(require=true,regex="^[1|2|3]$")
	private int type;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
