package com.cmge.ad.interfaces.resp;

import java.io.Serializable;
import java.util.List;

import com.cmge.ad.interfaces.vo.ResponseMsg;
import com.cmge.ad.interfaces.vo.ParentMenu;

public class MenuListResp extends ResponseMsg implements Serializable{

	private List<ParentMenu> menuList;
	
	public List<ParentMenu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<ParentMenu> menuList) {
		this.menuList = menuList;
	}

	public MenuListResp() {
	}
	
	public MenuListResp(int resultCode) {
		setResultCode(resultCode);
	}

}
