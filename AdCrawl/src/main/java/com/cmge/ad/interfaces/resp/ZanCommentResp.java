package com.cmge.ad.interfaces.resp;

import com.cmge.ad.interfaces.vo.ResponseMsg;

public class ZanCommentResp extends ResponseMsg {

	public ZanCommentResp() {
	}
	
	public ZanCommentResp(int resultCode) {
		setResultCode(resultCode);
	}

}
