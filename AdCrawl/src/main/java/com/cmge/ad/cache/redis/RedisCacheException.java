package com.cmge.ad.cache.redis;

/**
 * @desc	缓存异常
 * @author	ljt
 * @time	2014-8-28 下午6:30:30
 */
public class RedisCacheException extends RuntimeException{
	
	public RedisCacheException() {
    }
    
    public RedisCacheException(Throwable t) {
    	super(t);
    }

    public RedisCacheException(String message) {
    	super(message);
    }
	
}
