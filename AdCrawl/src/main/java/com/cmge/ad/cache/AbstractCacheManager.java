package com.cmge.ad.cache;

public abstract class AbstractCacheManager implements CacheManagerI{
	
	/** 缓存区默认名  **/
	public static final String DEFAULT_REGION = "DefaultRegion";
	
	/** 块名不能为null异常提示 **/
	public static final String REGION_CANT_BE_NULL = "region can't be null";
	
	/** 缓存Key不能为null **/
    public static final String KEY_SHOULDT_BE_NULL = "key should't be null";
    
    /** 缓存Key和value不能为null **/
    public static final String VALUE_SHOULDT_BE_NULL = "key and value should't be null";
	
	public Object get(Object key) {
        return get(DEFAULT_REGION, key);
    }

    public void put(Object key, Object value) {
        put(DEFAULT_REGION, key, value);
    }

    public void delete(Object key) {
        delete(DEFAULT_REGION, key);
    }

    public Boolean replace(Object key, Object value) {
        return replace(DEFAULT_REGION, key, value);
    }

    public Boolean putIfAbsent(Object key, Object value) {
        return putIfAbsent(DEFAULT_REGION, key, value);
    }
    
    public CacheConfig getCacheConfig() {
    	return null;
    }
    
    public void setCacheConfig(CacheConfig cacheConfig) {
    	
    }
	
}
