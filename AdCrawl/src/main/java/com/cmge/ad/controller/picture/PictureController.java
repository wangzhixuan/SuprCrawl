package com.cmge.ad.controller.picture;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cmge.ad.controller.BaseController;
import com.cmge.ad.model.Album;
import com.cmge.ad.model.AlbumStore;
import com.cmge.ad.model.JsonResult;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Picture;
import com.cmge.ad.model.Result;
import com.cmge.ad.service.PictureService;
import com.cmge.ad.util.Constant;
import com.cmge.ad.util.Pager;
import com.cmge.ad.util.SuprUtil;

/**
 * @desc	图片仓库管理
 * @author	ljt
 * @time	2015-1-27 下午4:40:18
 */
@Controller
public class PictureController extends BaseController{
	
	public static final Logger logger = Logger.getLogger(PictureController.class);
	
	@Autowired
	private PictureService pictureService;
	
	/**
	 * 跳转图片仓库列表
	 */
	@RequestMapping("/picStoreList")
	public String picStoreList(){
		return "/adCrawl/picture/picture_store_list";
	}
	
	/**
	 * 跳转图片列表
	 */
	@RequestMapping("/picList")
	public String picList(){
		return "/adCrawl/picture/picture_list";
	}
	
	/**
	 * 图片仓库列表
	 */
	@RequestMapping("/getPictureStoreList")
	public @ResponseBody
	JsonResult getPictureStoreList(HttpSession session,Pager<AlbumStore> pager,AlbumStore album) throws UnsupportedEncodingException {
		JsonResult result = new JsonResult();
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		
		if(!StringUtils.isEmpty(album.getTitle())){
			album.setTitle(URLDecoder.decode(album.getTitle(), "utf-8"));
		}
		
		paramMap.put("album", album);
		pictureService.getPictureStoreList(pager,paramMap);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
	/**
	 * 图片列表
	 */
	@RequestMapping("/getPictureList")
	public @ResponseBody
	JsonResult getPictureList(HttpSession session,Pager<Album> pager,Album album) throws UnsupportedEncodingException {
		JsonResult result = new JsonResult();
		HashMap<String, Object> paramMap = new HashMap<String,Object>();
		
		if(!StringUtils.isEmpty(album.getTitle())){
			album.setTitle(URLDecoder.decode(album.getTitle(), "utf-8"));
		}
		
		paramMap.put("album", album);
		pictureService.getPictureList(pager,paramMap);
		result.setData(pager.getRows());
		result.setTotalCount(pager.getTotalCount());
		return result;
	}
	
	/**
	 * 跳转相册详情页面
	 */
	@RequestMapping("/getPictureStoreInfoById")
	public String getPictureStoreInfoById(ModelMap map,String id) {
		AlbumStore album = pictureService.getPictureStoreInfoById(id);
		List<Picture> picList = pictureService.getPictureStoreList(id);
		album.setPicList(picList);
		
		// 获取段子的子分类Menu
		List<Menu> menuList = pictureService.getPictureChildMenu();

		map.addAttribute("menuList", menuList);
		map.addAttribute("album", album);
		return "/adCrawl/picture/picture_store_info";
	}
	
	/**
	 * 跳转相册详情页面
	 */
	@RequestMapping("/getPictureInfoById")
	public String getPictureInfoById(HttpServletRequest request,ModelMap map,String id) {
		Album album = pictureService.getPictureInfoById(id);
		List<Picture> picList = pictureService.getPictureListById(id);
		
		String path = request.getContextPath();
		String basePath = request.getScheme() + "://"
				+ request.getServerName() + ":" + request.getServerPort()
				+ path;
		
		if(!SuprUtil.isEmptyCollection(picList)){
			for(Picture pic : picList){
				pic.setLocationUrl(basePath+pic.getLocationUrl());
			}
		}
		
		album.setPictureList(picList);
		
		map.addAttribute("album", album);
		return "/adCrawl/picture/picture_info";
	}
	
	/**
	 * 删除单个仓库相册
	 */
	@RequestMapping("/deletePictureStoreById")
	public @ResponseBody
	Result deletePictureStoreById(Integer id){
		pictureService.deletePictureStoreById(id);
		return new Result("success", Constant.DEAL_SUCCESS);
	}
	
	/**
	 * 获取相册的下一个或上一个Id
	 */
	@RequestMapping("/getPictureStoreId")
	public @ResponseBody
	JsonResult getPictureStoreId(AlbumStore album) throws UnsupportedEncodingException{
		JsonResult result = new JsonResult();
		
		if(!StringUtils.isEmpty(album.getTitle())){
			album.setTitle(URLDecoder.decode(album.getTitle(), "utf-8"));
		}
		
		String id = pictureService.getPictureStoreId(album);
		result.setData(id);
		return result;
	}
	
	/**
	 * 获取相册的下一个或上一个Id
	 */
	@RequestMapping("/getPictureId")
	public @ResponseBody
	JsonResult getPictureId(Album album) throws UnsupportedEncodingException{
		JsonResult result = new JsonResult();
		
		String id = pictureService.getPictureId(album);
		result.setData(id);
		return result;
	}
	
	/**
	 * 同步仓库图片
	 */
	@RequestMapping("/syncPictureStore")
	public @ResponseBody
	Result syncPictureStore(Integer id,Integer menuId){
		if(!pictureService.existSyncPicture(id)){
			pictureService.syncPictureStore(id,menuId);
			return new Result("success", Constant.DEAL_SUCCESS);
		}else{
			return new Result("fail", Constant.DEAL_FAIL);
		}
	}
	
	/**
	 * 相册菜单列表
	 */
	@RequestMapping("/getAllPictureMenu")
	public @ResponseBody
	JsonResult getAllPictureMenu(){
		JsonResult result = new JsonResult();
		List<Menu> menuList = pictureService.getAllPictureMenu();

		Menu menu = new Menu();
		menu.setId(-1);
		menu.setMenuName("===请选择===");
		menuList.add(0,menu);
		
		result.setData(menuList);
		return result;
	}
	
	/**
	 * 获取图片分类
	 */
	@RequestMapping("/getPicMenuList")
	public String getPicMenuList(ModelMap map) {
		// 获取段子的子分类Menu
		List<Menu> menuList = pictureService.getPictureChildMenu();
		
		map.addAttribute("menuList", menuList);
		return "/adCrawl/picture/picture_store_batchsync";
	}
	
	/**
	 * 批量同步仓库相册
	 */
	@RequestMapping("/batchSyncAlbumStore")
	public @ResponseBody
	Result batchSyncAlbumStore(String ids,Integer menuId){
		String[] id = ids.split(",");
		if(!SuprUtil.isEmptyStringArray(id)){
			for (String syncId : id) {
				pictureService.syncPictureStore(Integer.parseInt(syncId),menuId);
			}
			return new Result("success", Constant.DEAL_SUCCESS);
		}else{
			return new Result("error", Constant.DEAL_FAIL);
		}
	}
	
}
