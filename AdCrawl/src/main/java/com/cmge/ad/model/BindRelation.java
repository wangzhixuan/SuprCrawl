package com.cmge.ad.model;


/**
 * @desc 绑定关系
 * @author liming
 * @time 2015-3-4
 */
public class BindRelation {
	
	private Integer id;
	/**栏目id**/
    private String menuId;
    /**父类栏目id**/
    private String parentId;
    /**栏目名称**/
    private String menuName;
    /**分类id**/
    private String siteCategoryId;
    /**分类名称**/
    private String categoryName;
    /**站点名称**/
    private String siteId;
    private String siteName;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getSiteCategoryId() {
		return siteCategoryId;
	}
	public void setSiteCategoryId(String siteCategoryId) {
		this.siteCategoryId = siteCategoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	@Override
	public String toString() {
		return "BindRelation [id=" + id + ", menuId=" + menuId + ", parentId="
				+ parentId + ", menuName=" + menuName + ", siteCategoryId="
				+ siteCategoryId + ", categoryName=" + categoryName
				+ ", siteId=" + siteId + ", siteName=" + siteName + "]";
	}
	
}
