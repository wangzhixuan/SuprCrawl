package com.cmge.ad.model;

import java.util.List;

/**
 * @desc	应用
 * @author	ljt
 * @time	2015-2-7 下午3:59:26
 */
public class App {
	
	// 原始页面url
	private String remoteUrl;
	
	// 唯一标识
	private String uniqueId;
	
	// 公司
	private String company;
	
	// 应用名称
	private String appName;
	
	// 游戏icon
	private String iconUrl;
	
	// 下载地址
	private String downloadUrl;
	
	// 版本大小
	private String appSize;
	
	// 版本号
	private String appVersion;
	
	// 最后更新时间
	private String updateTime;
	
	// 包名
	private String packageName;
	
	// 描述
	private String description;
	
	// 权限列表
	private List<String> authList;
	
	// 游戏截图列表
	private List<String> picList;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getAppSize() {
		return appSize;
	}

	public void setAppSize(String appSize) {
		this.appSize = appSize;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getAuthList() {
		return authList;
	}

	public void setAuthList(List<String> authList) {
		this.authList = authList;
	}

	public List<String> getPicList() {
		return picList;
	}

	public void setPicList(List<String> picList) {
		this.picList = picList;
	}

	public String getRemoteUrl() {
		return remoteUrl;
	}

	public void setRemoteUrl(String remoteUrl) {
		this.remoteUrl = remoteUrl;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}
