package com.cmge.ad.model;

import java.util.List;

public class PaintResp {
	
	// 画作列表
	private List<LockPaint> paintList;	
	
	// 处理结果
	private int resultCode;	
	
	// 处理失败原因
	private String errMsg;
	
	public PaintResp(int resultCode) {
		super();
		this.resultCode = resultCode;
	}

	public int getResultCode() {
		return resultCode;
	}

	public void setResultCode(int resultCode) {
		this.resultCode = resultCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public List<LockPaint> getPaintList() {
		return paintList;
	}

	public void setPaintList(List<LockPaint> paintList) {
		this.paintList = paintList;
	}		
}
