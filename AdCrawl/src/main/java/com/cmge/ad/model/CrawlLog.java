package com.cmge.ad.model;

import java.io.Serializable;

/**
 * @desc	爬虫日志
 * @author	ljt
 * @time	2015-2-1 下午6:10:01
 */
public class CrawlLog implements Serializable{
	
	private static final long serialVersionUID = 1627393030004443820L;

	private String time;
	
	private String log;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}
}
