package com.cmge.ad.model;

/**
 * @desc	图片仓库
 * @author	ljt
 * @time	2015-2-3 下午5:13:52
 */
public class PictureStore {
	
	private Integer id;
	
	private Integer albumId;
	
	private String minImageUrl;
	
	private String maxImageUrl;
	
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlbumId() {
		return albumId;
	}

	public void setAlbumId(Integer albumId) {
		this.albumId = albumId;
	}

	public String getMinImageUrl() {
		return minImageUrl;
	}

	public void setMinImageUrl(String minImageUrl) {
		this.minImageUrl = minImageUrl;
	}

	public String getMaxImageUrl() {
		return maxImageUrl;
	}

	public void setMaxImageUrl(String maxImageUrl) {
		this.maxImageUrl = maxImageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
