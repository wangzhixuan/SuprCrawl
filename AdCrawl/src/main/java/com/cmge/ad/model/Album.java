package com.cmge.ad.model;

import java.util.List;

/**
 * @desc	相册
 * @author	ljt
 * @time	2014-12-30 下午8:30:53
 */
public class Album {

	private Integer id;
	
	private String albumName;
	
	private String albumUrl;
	
	private String menuName;
	
	private String title;
	
	private Integer menuId;
	
	private int zan;
	
	private int orient;
	
	private List<Picture> pictureList;
	
	public int getOrient() {
		return orient;
	}

	public void setOrient(int orient) {
		this.orient = orient;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public List<Picture> getPictureList() {
		return pictureList;
	}

	public void setPictureList(List<Picture> pictureList) {
		this.pictureList = pictureList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getAlbumUrl() {
		return albumUrl;
	}

	public void setAlbumUrl(String albumUrl) {
		this.albumUrl = albumUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	@Override
	public String toString() {
		return "Album [albumUrl=" + albumUrl + ", pictureList=" + pictureList + "]";
	}
}
