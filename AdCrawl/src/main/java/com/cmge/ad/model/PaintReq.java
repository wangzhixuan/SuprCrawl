package com.cmge.ad.model;

public class PaintReq {
	
	// 最近获取的画作Id
	private Integer lastPaintId;
	
	// 画作级别（1:所有人群  2:18岁以上人群  3:限制级别）
	private Integer level;
	
	// 移动设备国际身份码
	private String IMEI;     
	
	// 移动用户识别码
	private String IMSI;	
	
	// 渠道号
	private String channel; 
	
	// 手机型号
	private String model;    
	
	public Integer getLastPaintId() {
		return lastPaintId;
	}

	public void setLastPaintId(Integer lastPaintId) {
		this.lastPaintId = lastPaintId;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public String getIMSI() {
		return IMSI;
	}

	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}
	
}
