package com.cmge.ad.model;

public class LockPaint {
	
	// 唯一标识ID
	private Integer id;
	
	// 画作名称
	private String paintName;    
	
	// 画作简介
	private String paintDesc;	 
	
	// 画作链接地址
	private String paintUrl;	 
	
	private Integer level;
	
	// 画家姓名
	private String paintist;	  
	
	// 国籍
	private String nationality;   
	
	// 生年
	private String birthYear;     
	
	// 卒年
	private String deathYear;
	
	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPaintName() {
		return paintName;
	}

	public void setPaintName(String paintName) {
		this.paintName = paintName;
	}

	public String getPaintDesc() {
		return paintDesc;
	}

	public void setPaintDesc(String paintDesc) {
		this.paintDesc = paintDesc;
	}

	public String getPaintUrl() {
		return paintUrl;
	}

	public void setPaintUrl(String paintUrl) {
		this.paintUrl = paintUrl;
	}

	public String getPaintist() {
		return paintist;
	}

	public void setPaintist(String paintist) {
		this.paintist = paintist;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public String getDeathYear() {
		return deathYear;
	}

	public void setDeathYear(String deathYear) {
		this.deathYear = deathYear;
	}
	
}
