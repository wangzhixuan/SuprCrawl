package com.cmge.ad.model;


/**
 * @desc	视频
 * @author	ljt
 * @time	2014-12-30 下午6:29:32
 */
public class Video {
	
	// 作者
	private String author;
	
	// 文件播放地址
	private String fileUrl;
	
	// 视频标题
	private String title;
	
	// 顶
	private int ding;
	
	// 踩
	private int cai;
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getDing() {
		return ding;
	}

	public void setDing(int ding) {
		this.ding = ding;
	}

	public int getCai() {
		return cai;
	}

	public void setCai(int cai) {
		this.cai = cai;
	}

}
