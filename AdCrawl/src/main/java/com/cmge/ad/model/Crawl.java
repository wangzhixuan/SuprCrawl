package com.cmge.ad.model;

/**
 * @desc	爬虫任务
 * @author	ljt
 * @time	2015-1-31 下午2:40:41
 */
public class Crawl {
	
	private Integer id;
	
	private int type;
	
	private Integer siteId;
	
	private String siteName;
	
	private String categoryName;
	
	private Integer siteCategoryId;
	
	private String listUrl;
	
	private int start;
	
	private int end;
	
	private String infoUrlXpath;
	
	private String infoUrlPre;
	
	private String nextUrlPre;
	
	private String infoUrlRegular;
	
	private String listUrlRegular;
	
	private String infoListNextXpath;
	
	private String titleXpath;
	
	private String infoRepeatXpath;
	
	private String albumIdSign;
	
	private String infoContentXpath;
	
	private String infoPicXpath;
	
	private String repeatFlagXpath;
	
	private String infoListStart;
	
	private int repeatMaxTimes;
	
	private int crawlTime;
	
	private String infoFirstRegular;
	
	// 最后一次成功抓取时间
	private String lastCrawlTime;
	
	// 每次抓取休眠时间
	private int sleepTime;
	
	// 任务运行状态  0：初始 1：运行中 2：运行失败 3：运行完成
	private int statue;
	
	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getSleepTime() {
		return sleepTime;
	}

	public void setSleepTime(int sleepTime) {
		this.sleepTime = sleepTime;
	}

	public String getInfoUrlRegular() {
		return infoUrlRegular;
	}

	public void setInfoUrlRegular(String infoUrlRegular) {
		this.infoUrlRegular = infoUrlRegular;
	}

	public String getLastCrawlTime() {
		return lastCrawlTime;
	}

	public void setLastCrawlTime(String lastCrawlTime) {
		this.lastCrawlTime = lastCrawlTime;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public Integer getSiteCategoryId() {
		return siteCategoryId;
	}

	public void setSiteCategoryId(Integer siteCategoryId) {
		this.siteCategoryId = siteCategoryId;
	}

	public String getListUrl() {
		return listUrl;
	}

	public void setListUrl(String listUrl) {
		this.listUrl = listUrl;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public String getInfoUrlXpath() {
		return infoUrlXpath;
	}

	public void setInfoUrlXpath(String infoUrlXpath) {
		this.infoUrlXpath = infoUrlXpath;
	}

	public String getInfoUrlPre() {
		return infoUrlPre;
	}

	public void setInfoUrlPre(String infoUrlPre) {
		this.infoUrlPre = infoUrlPre;
	}

	public String getInfoContentXpath() {
		return infoContentXpath;
	}

	public void setInfoContentXpath(String infoContentXpath) {
		this.infoContentXpath = infoContentXpath;
	}

	public String getInfoPicXpath() {
		return infoPicXpath;
	}

	public void setInfoPicXpath(String infoPicXpath) {
		this.infoPicXpath = infoPicXpath;
	}

	public String getRepeatFlagXpath() {
		return repeatFlagXpath;
	}

	public void setRepeatFlagXpath(String repeatFlagXpath) {
		this.repeatFlagXpath = repeatFlagXpath;
	}

	public int getRepeatMaxTimes() {
		return repeatMaxTimes;
	}

	public void setRepeatMaxTimes(int repeatMaxTimes) {
		this.repeatMaxTimes = repeatMaxTimes;
	}

	public int getCrawlTime() {
		return crawlTime;
	}

	public void setCrawlTime(int crawlTime) {
		this.crawlTime = crawlTime;
	}

	public String getListUrlRegular() {
		return listUrlRegular;
	}

	public void setListUrlRegular(String listUrlRegular) {
		this.listUrlRegular = listUrlRegular;
	}

	public String getInfoListStart() {
		return infoListStart;
	}

	public void setInfoListStart(String infoListStart) {
		this.infoListStart = infoListStart;
	}

	public String getInfoListNextXpath() {
		return infoListNextXpath;
	}

	public void setInfoListNextXpath(String infoListNextXpath) {
		this.infoListNextXpath = infoListNextXpath;
	}

	public String getInfoRepeatXpath() {
		return infoRepeatXpath;
	}

	public void setInfoRepeatXpath(String infoRepeatXpath) {
		this.infoRepeatXpath = infoRepeatXpath;
	}

	public String getAlbumIdSign() {
		return albumIdSign;
	}

	public void setAlbumIdSign(String albumIdSign) {
		this.albumIdSign = albumIdSign;
	}

	public String getInfoFirstRegular() {
		return infoFirstRegular;
	}

	public void setInfoFirstRegular(String infoFirstRegular) {
		this.infoFirstRegular = infoFirstRegular;
	}

	public String getTitleXpath() {
		return titleXpath;
	}

	public void setTitleXpath(String titleXpath) {
		this.titleXpath = titleXpath;
	}

	public String getNextUrlPre() {
		return nextUrlPre;
	}

	public void setNextUrlPre(String nextUrlPre) {
		this.nextUrlPre = nextUrlPre;
	}
}
