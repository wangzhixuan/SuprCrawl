package com.cmge.ad.model;

import java.util.List;

/**
 * @desc	相册仓库
 * @author	ljt
 * @time	2015-1-27 下午4:54:15
 */
public class AlbumStore {
	
	private Integer id;
	
	// 相册名称
	private String albumName;
	
	// 相册唯一标识
	private String uniqueId;
	
	// 封面url
	private String albumUrl;
	
	// 相册标题
	private String title;
	
	// 抓取时间
	private String createTime;
	
	private Integer siteId;
	
	private Integer siteCategoryId;
	
	private int orient;
	
	private int statue;
	
	private int menuId;
	
	private int zan;
	
	// 图片列表
	private List<Picture> picList;
	
	public int getZan() {
		return zan;
	}

	public void setZan(int zan) {
		this.zan = zan;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public int getOrient() {
		return orient;
	}

	public void setOrient(int orient) {
		this.orient = orient;
	}

	public Integer getSiteCategoryId() {
		return siteCategoryId;
	}

	public void setSiteCategoryId(Integer siteCategoryId) {
		this.siteCategoryId = siteCategoryId;
	}

	public Integer getSiteId() {
		return siteId;
	}

	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}

	public List<Picture> getPicList() {
		return picList;
	}

	public void setPicList(List<Picture> picList) {
		this.picList = picList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getAlbumUrl() {
		return albumUrl;
	}

	public void setAlbumUrl(String albumUrl) {
		this.albumUrl = albumUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
}
