package com.cmge.ad.util.thread;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.cmge.ad.service.ArticleService;
import com.cmge.ad.util.context.SpringContextUtil;

/**
 * @desc	自动同步段子
 * @author	ljt
 * @time	2015-1-29 下午3:50:46
 */
public class AutoSyncArticle implements Runnable {

	public static final Logger logger = Logger.getLogger(AutoSyncArticle.class);
	
	private ArticleService articleService;
	
	private int size = 10;
	
	public void run() {
		ApplicationContext context = SpringContextUtil.getApplicationContext();
		articleService = (ArticleService) context.getBean(ArticleService.class);
		
		while (true) {
			try {
				logger.info("自动同步段子开始....");
				
				articleService.autoSyncArticle(size);
				
				logger.info("自动同步段子结束....");
				
				// 休眠300秒
				Thread.sleep(300000);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
		}
	}

}
