package com.cmge.ad.util.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.configuration.PropertiesConfiguration;

import com.cmge.ad.util.config.CompositeFactory;
import com.cmge.ad.util.context.ServletContextUtil;
import com.cmge.ad.util.thread.AutoCrawlArticle;
import com.cmge.ad.util.thread.AutoSyncArticle;
import com.cmge.ad.util.thread.CrawlerMonitor;
import com.cmge.ad.util.thread.LoadLogFile;
import com.cmge.ad.util.thread.SyncAlbum;
import com.cmge.ad.util.thread.SyncArticle;

/**
 * @desc	系统监听器
 * @author	ljt
 * @time	2014-6-23 下午5:17:22
 */
public class SystemListener implements ServletContextListener{
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
	}
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		initConfig(event);
		initLog(event);
		ServletContextUtil.setContext(event.getServletContext());
		
//		initThread();
	}
	
	private void initThread() {
		Thread t1 = new Thread(new SyncArticle());
		t1.start();
		
		Thread t2 = new Thread(new SyncAlbum());
		t2.start();
		
		Thread t3 = new Thread(new CrawlerMonitor());
		t3.start();
		
		Thread t4 = new Thread(new AutoSyncArticle());
		t4.start();
		
		Thread t5 = new Thread(new AutoCrawlArticle());
		t5.start();
	}

	private void initConfig(ServletContextEvent event) {
		// 读取主配置文件  这里可以把properties和xml配置文件统一放在一个xml中，遍历加载
		try {
			PropertiesConfiguration propertiesConfiguration1 = new PropertiesConfiguration("resultCode.properties");
			PropertiesConfiguration propertiesConfiguration2 = new PropertiesConfiguration("systemConfig.properties");
			CompositeFactory.getInstance().addConfiguration(propertiesConfiguration1);
			CompositeFactory.getInstance().addConfiguration(propertiesConfiguration2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void initLog(ServletContextEvent event) {
		ServletContext context = event.getServletContext();
		String log4jParam = context.getInitParameter("log4jPath");
		String log4jPath = context.getRealPath(log4jParam);
		Thread thread = new Thread(new LoadLogFile(log4jPath));
		thread.start();
	}
}
