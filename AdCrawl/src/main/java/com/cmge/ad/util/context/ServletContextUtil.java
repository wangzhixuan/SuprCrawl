package com.cmge.ad.util.context;

import javax.servlet.ServletContext;

/**
 * @desc    获取Servlet上下文环境
 * @author  ljt
 * @time	2014-8-16 下午7:52:37
 */
public class ServletContextUtil{
	
	private static ServletContext context;
	
	public static void setContext(ServletContext servletContext){
		context = servletContext;
	}
	
	public static ServletContext getContext() { 
        return context; 
	} 
	
}
