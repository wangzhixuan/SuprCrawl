package com.cmge.ad.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @desc 操作时间统计线程绑定
 * @author ljt
 * @time 2014-7-31 下午5:21:36
 */
public class TimeUtils {
	private static ThreadLocal<Long> timeThreadLocal = new ThreadLocal<Long>();

	private static SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private static SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");

	public static String getTime(){
		return time.format(new Date());
	}
	
	public static String getDate(){
		return date.format(new Date());
	}
	
	public static void mark() {
		timeThreadLocal.set(System.currentTimeMillis());
	}

	public static long get() {
		return System.currentTimeMillis() - timeThreadLocal.get();
	}
	
	public static String formatTimeToStr(Date date){
		String timeStr = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		timeStr = formatter.format(date);
		return timeStr;
	} 
}
