package com.cmge.ad.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cmge.ad.model.Article;
import com.cmge.ad.model.ArticleStore;
import com.cmge.ad.model.Menu;
import com.cmge.ad.util.Pager;

/**
 * @desc	段子业务类
 * @author	ljt
 * @time	2014-10-15 下午5:58:37
 */
public interface ArticleService {

	void getArticleStoreList(Pager<ArticleStore> pager, HashMap<String, Object> paramMap);

	ArticleStore getArticleStoreInfoById(String id);

	String getArticleStoreId(ArticleStore article);

	void deleteArticleStoreById(Integer id);

	void syncArticleStore(Integer id,Integer menuId,Integer hot);

	boolean existSyncArticle(Integer id);

	void addArticle(ArticleStore article);

	void deleteSyncArticle(ArticleStore article);

	void updateArticleStoreStatue(ArticleStore article);

	List<ArticleStore> getSyncArticleList(int size);

	List<Menu> getArticleChildMenu();

	void getArticleList(Pager<Article> pager, HashMap<String, Object> paramMap,HttpServletRequest request);

	List<Menu> getAllArticleMenu();

	Article getArticleInfoById(String id);

	String getArticleId(Article article);

	void syncArticle(int size);

	int getArticleStoreCount();

	void saveArticle(Article article) throws FileNotFoundException, IOException;

	int delArticle(Integer id);

	void autoSyncArticle(int size);

	void refreshArticleCache();

}
