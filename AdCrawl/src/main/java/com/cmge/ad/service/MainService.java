package com.cmge.ad.service;

import javax.servlet.http.HttpServletRequest;

import com.cmge.ad.interfaces.req.AlbumListReq;
import com.cmge.ad.interfaces.req.ArticleListReq;
import com.cmge.ad.interfaces.req.CommentListReq;
import com.cmge.ad.interfaces.req.CommentReq;
import com.cmge.ad.interfaces.req.ConfigReq;
import com.cmge.ad.interfaces.req.FeedBackReq;
import com.cmge.ad.interfaces.req.PictureInfoReq;
import com.cmge.ad.interfaces.req.PictureListReq;
import com.cmge.ad.interfaces.req.RegisterReq;
import com.cmge.ad.interfaces.req.VersionUpdateReq;
import com.cmge.ad.interfaces.req.ZanCommentReq;
import com.cmge.ad.interfaces.req.ZanReq;
import com.cmge.ad.interfaces.resp.AlbumListResp;
import com.cmge.ad.interfaces.resp.ArticleListResp;
import com.cmge.ad.interfaces.resp.CommentListResp;
import com.cmge.ad.interfaces.resp.CommentResp;
import com.cmge.ad.interfaces.resp.ConfigResp;
import com.cmge.ad.interfaces.resp.FeedBackResp;
import com.cmge.ad.interfaces.resp.MenuListResp;
import com.cmge.ad.interfaces.resp.PictureInfoResp;
import com.cmge.ad.interfaces.resp.PictureListResp;
import com.cmge.ad.interfaces.resp.RegisterResp;
import com.cmge.ad.interfaces.resp.VersionUpdateResp;
import com.cmge.ad.interfaces.resp.ZanCommentResp;
import com.cmge.ad.interfaces.resp.ZanResp;
import com.cmge.ad.model.User;

/**
 * @desc	接口业务类
 * @author	ljt
 * @time	2014-10-15 下午5:58:37
 */
public interface MainService {

	ZanResp zan(ZanReq zanReq);

	MenuListResp getMenuList();

	ArticleListResp getArticle(ArticleListReq articleListReq,HttpServletRequest request);

	PictureListResp getPicture(PictureListReq picReq,HttpServletRequest request);

	FeedBackResp feedBack(FeedBackReq feedBackReq);

	VersionUpdateResp versionUpdate(VersionUpdateReq versionReq);

	ConfigResp getConfigValue(ConfigReq configReq);

	AlbumListResp getAlbum(AlbumListReq albumReq, HttpServletRequest request);

	PictureInfoResp getPictureInfo(PictureInfoReq picReq, HttpServletRequest request);

	RegisterResp register(RegisterReq registerReq);

	CommentResp comment(CommentReq commentReq);

	ZanCommentResp zanComment(ZanCommentReq zanReq);

	CommentListResp getCommentList(CommentListReq commentReq);

	boolean exist(User user);

	void addUser(User user);
}
