package com.cmge.ad.service;

import java.util.HashMap;
import java.util.List;

import com.cmge.ad.model.Album;
import com.cmge.ad.model.AlbumStore;
import com.cmge.ad.model.Menu;
import com.cmge.ad.model.Picture;
import com.cmge.ad.util.Pager;

/**
 * @desc	相册业务类
 * @author	ljt
 * @time	2014-10-15 下午5:58:37
 */
public interface PictureService {

	void getPictureStoreList(Pager<AlbumStore> pager, HashMap<String, Object> paramMap);

	AlbumStore getPictureStoreInfoById(String id);

	List<Picture> getPictureList(String id);

	String getPictureStoreId(AlbumStore album);

	void deletePictureStoreById(Integer id);

	List<Menu> getPictureChildMenu();

	boolean existSyncPicture(Integer id);

	void syncPictureStore(Integer id, Integer menuId);

	List<AlbumStore> getSyncAlbumList();

	void deleteSyncAlbum(AlbumStore album);

	void updatePictureStoreStatue(AlbumStore album);

	List<Picture> getPictureListByAlbumId(Integer id);

	void addAlbum(AlbumStore album);

	void addPicture(Picture pic);

	void syncAlbum();

	void getPictureList(Pager<Album> pager, HashMap<String, Object> paramMap);

	List<Menu> getAllPictureMenu();

	List<Picture> getPictureStoreList(String id);

	Album getPictureInfoById(String id);

	List<Picture> getPictureListById(String id);

	String getPictureId(Album album);
}
